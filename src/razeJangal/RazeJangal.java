/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package razeJangal;

import java.io.File;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import razeJangal.game.rule.Game;
import razeJangal.graphics.GraphicalGame;
import razeJangal.graphics.server.GraphicalServer;

/**
 *
 * @author m.a.b.shemirani
 */
public class RazeJangal extends Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("RAZ E JANGAL");

        primaryStage.setScene(mainMenuScene(primaryStage));
        primaryStage.show();
}

    public Scene mainMenuScene(final Stage primaryStage)
    {
        StackPane root = new StackPane();
        Scene scene = new Scene(root, 400, 400);
        primaryStage.setResizable(false);
        
        root.setStyle("-fx-background-color: #c6a800;");
        Button startButton = new Button("Start Game");
        startButton.setFont(new Font(50));

        startButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                primaryStage.setScene(selectMenuScene(primaryStage));
            }
        });
        root.getChildren().add(startButton);
        return scene;
    }
    File map = new File("src/razeJangal/graphics/resources/map/default.map");
    File card = new File("src/razeJangal/graphics/resources/card/movie.set");
    TextField[] names = new TextField[4];

    public Scene selectMenuScene(final Stage primaryStage)
    {
        TilePane root = new TilePane();
        Scene scene = new Scene(root, 400, 550);
        primaryStage.setResizable(false);
        root.setPrefColumns(1);
        root.setAlignment(Pos.CENTER);
        root.setStyle("-fx-background-color: #c6a800;");
        
        for (int i = 0 ; i < 4; i++)
        {
            names[i] = new TextField("pl" + i);
            names[i].setStyle("-fx-font-size : 20;");
            root.getChildren().add(names[i]);
        }
        Button selectCardSet = new Button("Select Card Set");
        selectCardSet.setFont(new Font(30));
        
        Button selectMap = new Button("Select Map");
        selectMap.setFont(new Font(30));
        
        Button startButton = new Button("Start Game");
        startButton.setFont(new Font(30));

        final FileChooser chooser = new FileChooser();
        
        selectMap.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                map = chooser.showOpenDialog(primaryStage);
            }
        });
        
        selectCardSet.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                card = chooser.showOpenDialog(primaryStage);
            }
        });
        

        startButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                if (names[0].getText().length() > 0 && names[1].getText().length() > 0 && map != null && card != null)
                {
                    
                    GraphicalGame game;
                    if (names[3].getText().length() > 0 )
                        game = new GraphicalGame(map, card, names[0].getText(), names[1].getText(), names[2].getText(), names[3].getText());
                    else if (names[2].getText().length() > 0)
                        game = new GraphicalGame(map, card, names[0].getText(), names[1].getText(), names[2].getText());
                    else
                        game = new GraphicalGame(map, card, names[0].getText(), names[1].getText());

                    primaryStage.setScene(game.getScene(primaryStage));
                    
                    
                    Thread logic = new Thread(new GameLogicThread(game, primaryStage));
                    logic.setDaemon(true);
                    logic.start();
                }
            }
        });
        root.setVgap(5);
        root.getChildren().add(selectCardSet);
        root.getChildren().add(selectMap);
        root.getChildren().add(startButton);
        
        return scene;
    }
}

class GameLogicThread extends Task
{
    GraphicalGame graphicGame;
    Stage primaryStage;
    public GameLogicThread(GraphicalGame game, Stage primaryStage)
    {
        graphicGame = game;
        this.primaryStage = primaryStage;
    }
    
    @Override
    protected Object call() throws Exception {        
                    System.out.print("1SFFS");
        Game game = new Game(graphicGame.getBoard(), graphicGame.getNumberOfPlayers());
                    System.out.print("2SFFS");
        GraphicalServer graphicServer = new GraphicalServer(primaryStage, graphicGame);
                    System.out.print("3SFFS");
        graphicServer.setGame(game);
                    System.out.print("4SFFS");
        game.startGame(graphicServer);
                    System.out.print("5SFFS");
        
                    System.out.print("6SFFS");
        while (game.getRound() < 14)
            graphicServer.play();

        return this;
    }
    
}