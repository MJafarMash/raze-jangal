package razeJangal.graphics.card;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import razeJangal.graphics.map.MapIO;

/**
 *
 * @author M.a.b.shemirani
 */
public class CardIO {
    public static void saveCard(CardGroup card, String filename)
    {
        try {
            Formatter formatter = new Formatter(new File(filename + ".card"));
            formatter.format("%f %f", card.getMiniCardGroup().getLookAtX(), card.getMiniCardGroup().getLookAtY());
            MapIO.copyFile(card.getImageFilename(), filename + "_img." +card.getImageFilename().split("[.]")[1]);
            System.out.println(filename + "DFSD'" + card.getImageFilename());
            formatter.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CardIO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static CardGroup loadCard(String filename)
    {
        try {
            Scanner scanner = new Scanner(new File(filename));
            CardGroup card = new CardGroup(filename.split("[.]")[0] + "_img.jpg");
            card.setLookAt(scanner.nextDouble(), scanner.nextDouble());
            return card;
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            return null;
        }
                
    }
    
    public static void saveSetCard(CardSet set, String filename)
    {
        try {
            Formatter formatter = new Formatter(new File(filename + ".set"));
            formatter.close();
            for (int i = 0; i < 13; i++)
                saveCard(set.getCard(i), filename + "_" + i);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CardIO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void loadSetCard(CardSet set, String filename)
    {
        for (int i = 0; i < 13; i++)
            set.setCard(loadCard(filename.split("[.]")[0] + "_" + i + ".card"), i);
    }
}
