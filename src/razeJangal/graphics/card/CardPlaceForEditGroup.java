package razeJangal.graphics.card;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import razeJangal.graphics.map.MapPane;

/**
 *
 * @author M.a.b.shemirani
 */
public class CardPlaceForEditGroup extends CardPlaceGroup{
    private CardGroup cardGroup;

    public CardGroup getCard()
    {
        return cardGroup;
        
    }

    public void setCard(CardGroup cardGroup)
    {
//        System.out.println(cardGroup);
        this.cardGroup = cardGroup;
        getChildren().add(cardGroup);
    }
    
    public CardPlaceForEditGroup() {
        setBackgroundDraggable();
    }
    
    private void setBackgroundDraggable() {
        final CardPlaceForEditGroup cardPlace = this;
        setOnDragOver(new EventHandler<DragEvent>() {

            @Override
            public void handle(DragEvent event) {
                if (event.getGestureSource() != cardPlace &&(event.getDragboard().hasImage() || event.getDragboard().hasFiles()))
                {
                    event.acceptTransferModes(TransferMode.COPY);
                }
                
                event.consume();
              
            }
        });
        
        setOnDragDropped(new EventHandler<DragEvent>() {

            @Override
            public void handle(DragEvent event) {
                boolean success = false;
                Dragboard db = event.getDragboard();
                if (db.hasFiles())
                {
                    if (cardGroup == null)
                    {
                        cardGroup = new CardGroup(db.getFiles().get(0).getAbsolutePath());
                        cardPlace.getChildren().add(cardGroup);
                    }
                }
                event.setDropCompleted(success);

                event.consume();
            }
        });
    }
    protected void setDraggable()
    {
        // do not draggable
    }

}
