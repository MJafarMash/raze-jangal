package razeJangal.graphics.card;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import razeJangal.graphics.map.MapPane;

/**
 * graphical group of place that card must be there.
 * 
 * @author M.a.b.shemirani
 */
public class CardPlaceGroup extends Group{
    private Group cardPlaceGroup = new Group();
    private MapPane mapPane;
    // <for dragging>
    private double mouseX;
    private double mouseY;
    private double lastX;
    private double lastY;
    // </for dragging>

    public static final double WIDTH = 150;
    public static final double HEIGHT = 250;
    
    public CardPlaceGroup()
    {
        getChildren().add(cardPlaceGroup);
        draw();
        setDraggable();
    }
    
    private void draw()
    {
        Rectangle rectangle = new Rectangle(-WIDTH / 2, -HEIGHT /2, WIDTH, HEIGHT);
        rectangle.setCache(true);
        
        final double ARC_VALUE = 15.0;
        rectangle.setArcHeight(ARC_VALUE);
        rectangle.setArcWidth(ARC_VALUE);
       
        rectangle.setFill(Color.GOLD);
    
        rectangle.setStroke(Color.GOLDENROD);
        rectangle.setStrokeType(StrokeType.OUTSIDE);
        rectangle.setStrokeWidth(10);
        
        rectangle.setEffect(new GaussianBlur());
        setEffect(new DropShadow(10, Color.AQUA));
        getChildren().add(rectangle);
        
        move(150, 150);
    }
    public void move(double x, double y)
    {
        setLayoutX(x);
        setLayoutY(y);
    }
    
    protected void setDraggable()
    {
        setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                mouseX = event.getSceneX();
                mouseY = event.getSceneY();
                
                lastX = getLayoutX();
                lastY = getLayoutY();
            }
        });
        
        setOnMouseDragged(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                lastX += event.getSceneX() - mouseX;
                lastY += event.getSceneY() - mouseY;
                
                if (mapPane.isEditMode())
                {
                    if (lastX > CardPlaceGroup.WIDTH / 2  && lastY > CardPlaceGroup.HEIGHT / 2)
                        move(lastX, lastY);
                }
                mouseX = event.getSceneX();
                mouseY = event.getSceneY();
            }
        });
    }
    
    public void setMap(MapPane mapPane)
    {
        this.mapPane = mapPane;
    }
}