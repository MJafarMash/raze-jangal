package razeJangal.graphics.card;

import java.io.File;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author M.a.b.shemirani
 */
public class CardEditor extends Application{

    public static void main(String[] args)
    {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Cart Editor");
        
        BorderPane root = new BorderPane();

        TilePane tilePane = new TilePane(Orientation.HORIZONTAL);
        tilePane.setPrefColumns(7);
        tilePane.setPrefRows(2);
        root.setCenter(tilePane);

        TilePane bottomOfScene = new TilePane();
        Button save = new Button("SAVE");
        Button load = new Button("LOAD");
        
        bottomOfScene.getChildren().add(save);
        bottomOfScene.getChildren().add(load);
        
        root.setBottom(bottomOfScene);
        final CardPlaceForEditGroup cardPlace[] = new CardPlaceForEditGroup[13];
        for (int i = 0; i < 13; i++)
        {
            cardPlace[i] = new CardPlaceForEditGroup();
            tilePane.getChildren().add(cardPlace[i]);
        }

        final Stage stage = primaryStage;
        save.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                CardSet set = new CardSet();
                for (int i = 0; i < 13; i++)
                    set.setCard(cardPlace[i].getCard(), i);
                FileChooser chooser = new FileChooser();
                File path = chooser.showSaveDialog(stage);
                CardIO.saveSetCard(set, path.getAbsolutePath());
            }
        });

        load.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                CardSet set = new CardSet();
                FileChooser chooser = new FileChooser();
                File path = chooser.showOpenDialog(stage);
                CardIO.loadSetCard(set, path.getAbsolutePath());
                
                for (int i = 0; i < 13; i++)
                    cardPlace[i].setCard(set.getCard(i));
            }
        });
        
        Scene scene = new Scene(root, 1400, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
}
