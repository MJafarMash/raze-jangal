package razeJangal.graphics.card;

import java.io.File;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;

/**
 * graphical group for cards
 * @author M.a.b.shemirani
 */
public class CardGroup extends Group {
    private MiniCardGroup miniCardGroup;
    public static final double WIDTH = 150;
    public static final double HEIGHT = 250;
    private String imageFilename;
    private boolean isEditMode = true;
    
    public CardGroup(String imageFilename)
    {
        this.imageFilename = imageFilename;
        miniCardGroup = new MiniCardGroup(this);
        draw();
        setLookAtChangable();
    }
    
    public void move(double x, double y)
    {
        setLayoutX(x);
        setLayoutY(y);
    }
    private void setLookAtChangable()
    {
        setOnMouseMoved(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if(event.isAltDown())
                {
                    if (isEditMode)
                        miniCardGroup.setLookAt(event.getX(), event.getY());
                }
            }
        });
    }
    public void setEditMode(boolean isEditMode)
    {
        this.isEditMode = isEditMode;
    }
    private void draw()
    {
        ImageView imageView;
    
        try{
            imageView = new ImageView(new Image(CardGroup.class.getResource(imageFilename).toExternalForm()));
        }catch(Exception e)
        {
            imageView = new ImageView(new Image(new File(imageFilename).toURI().toString()));
        }
        imageView.setLayoutX(-WIDTH/2 );
        imageView.setLayoutY(-HEIGHT/2);
        
        imageView.setFitHeight(HEIGHT);
        imageView.setFitWidth(WIDTH);
        
        imageView.setSmooth(true);
        imageView.setCache(true);
        
        Rectangle borderRectangle = new Rectangle(0 , 0, WIDTH, HEIGHT);
        
        final double ARC_VALUE = 15.0;
        borderRectangle.setArcHeight(ARC_VALUE);
        borderRectangle.setArcWidth(ARC_VALUE);
        
        imageView.setClip(borderRectangle);
        
        getChildren().add(imageView);
        getChildren().add(miniCardGroup);
    }
 
    public String getImageFilename()
    {
        return imageFilename;
    }
    public void setLookAt(double x, double y)
    {
        miniCardGroup.setLookAt(x, y);
    }
    public MiniCardGroup getMiniCardGroup()
    {
        MiniCardGroup newMiniCardGroup = miniCardGroup.getOnCopyOfThis();
        newMiniCardGroup.setOpacity(0);
        return newMiniCardGroup;
    }
}