package razeJangal.graphics.card;

import java.io.File;
import javafx.animation.FadeTransition;
import javafx.scene.Group;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

/**
 * class for graphical circle that zoom in on point of card
 * @author M.a.b.shemirani
 */
public class MiniCardGroup extends Group{
    private CardGroup cardGroup;
    
    private static final double RADIUS = 30;
    public static final double POS_X_RELATIVE_CARDGROUP = 40;
    public static final double POS_Y_RELATIVE_CARDGROUP = -90;
    
    private double lookAtX;
    private double lookAtY;
   
    private ImageView imageView = new ImageView();
    
    public MiniCardGroup(CardGroup cardGroup) {
        this.cardGroup = cardGroup;

        setPositionInCard();
        setLookAt(0, 0);
        draw();
    }

    public void setPositionOutsideCard()
    {
        setLayoutX(0);
        setLayoutY(0);
    }
    private void setPositionInCard()
    {
        setLayoutX(POS_X_RELATIVE_CARDGROUP);
        setLayoutY(POS_Y_RELATIVE_CARDGROUP);
    }
    private void draw()
    {
        try{
            imageView = new ImageView(new Image(MiniCardGroup.class.getResource(cardGroup.getImageFilename()).toExternalForm()));
        }catch(Exception e)
        {
            imageView = new ImageView(new Image(new File(cardGroup.getImageFilename()).toURI().toString()));
        }
        imageView.setCache(true);
        imageView.setSmooth(true);
        
        imageView.setFitHeight(CardGroup.HEIGHT);
        imageView.setFitWidth(CardGroup.WIDTH);

        setLookAtX(lookAtX);
        setLookAtY(lookAtY);
        
        Circle borderCircle = new Circle(RADIUS);
        borderCircle.setCenterX(0);
        borderCircle.setCenterY(0);
        borderCircle.setStroke(Color.BLACK);
        borderCircle.setStrokeWidth(5);
        borderCircle.setEffect(new DropShadow(10, Color.CHOCOLATE));
        borderCircle.setCache(true);
        borderCircle.setSmooth(true);
        
        Circle circleClip = new Circle(RADIUS);
        circleClip.setCenterX(0);
        circleClip.setCenterY(0);

        imageView.setClip(circleClip);

        getChildren().add(borderCircle);
        getChildren().add(imageView);        
    }
    final public void setLookAt(double lookAtX, double lookAtY)
    {
        setLookAtX(lookAtX);
        setLookAtY(lookAtY);
    }
    public double getLookAtX()
    {
        return lookAtX;
    }
    public double getLookAtY()
    {
        return lookAtY;
    }
    public void setLookAtX(double lookAtX)
    {
        if (lookAtX < RADIUS - CardGroup.WIDTH / 2)
            this.lookAtX = RADIUS - CardGroup.WIDTH / 2;
        else if (lookAtX > -RADIUS + CardGroup.WIDTH / 2)
            this.lookAtX = -RADIUS + CardGroup.WIDTH / 2;
        else
            this.lookAtX = lookAtX;
        imageView.setX(-this.lookAtX - CardGroup.WIDTH / 2);
    }

    public void setLookAtY(double lookAtY)
    {
        if (lookAtY < RADIUS - CardGroup.HEIGHT / 2)
            this.lookAtY = RADIUS - CardGroup.HEIGHT / 2;
        else if (lookAtY > -RADIUS + CardGroup.HEIGHT / 2)
            this.lookAtY = -RADIUS + CardGroup.HEIGHT / 2;
        else
            this.lookAtY = lookAtY;
        imageView.setY(-this.lookAtY - CardGroup.HEIGHT / 2);
    }
    
    public MiniCardGroup getOnCopyOfThis()
    {
        MiniCardGroup miniCardGroup = new MiniCardGroup(cardGroup);
        miniCardGroup.setPositionOutsideCard();
        miniCardGroup.setLookAt(lookAtX, lookAtY);
        return miniCardGroup;
    }
    
    public void setToShow()
    {
        FadeTransition transition = new FadeTransition(Duration.millis(2000));
        transition.setFromValue(0);
        transition.setToValue(1);
        transition.setNode(this);
        transition.setAutoReverse(false);
        transition.setCycleCount(1);
        
        transition.play();
        
    }
    
    public void setShowAndHide()
    {
        FadeTransition transition = new FadeTransition(Duration.millis(4000));
        transition.setFromValue(0);
        transition.setToValue(1);
        transition.setNode(this);
        transition.setAutoReverse(true);
        transition.setCycleCount(2);
        
        transition.play();
    }
    public void setToHide()
    {
        FadeTransition transition = new FadeTransition(Duration.millis(2000));
        transition.setFromValue(1);
        transition.setToValue(0);
        transition.setNode(this);
        transition.setAutoReverse(false);
        transition.setCycleCount(1);
        
        transition.play();
    }
}
