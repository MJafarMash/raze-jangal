package razeJangal.graphics.card;

/**
 *
 * @author M.a.b.shemirani
 */
public class CardSet {
    private CardGroup[] set = new CardGroup[13];
    
    public CardGroup getCard(int i)
    {
        return set[i];
    }
    
    public void setCard(CardGroup card, int i)
    {
        set[i] = card;
    }
    
    public CardGroup getCard(String s)
    {
        int no;
        no = s.charAt(0) - 'A';
        
        if (no > 12 || no < 0)
            no = 0;
        set[no].setEditMode(false);
        return set[no];
    }
}
