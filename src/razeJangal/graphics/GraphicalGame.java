package razeJangal.graphics;

import java.io.File;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import razeJangal.game.DicePool;
import razeJangal.game.board.Board;
import razeJangal.graphics.card.CardIO;
import razeJangal.graphics.card.CardSet;
import razeJangal.graphics.map.MapIO;
import razeJangal.graphics.map.MapPane;
import razeJangal.graphics.player.PlayerGroup;

/**
 *
 * @author M.a.b.sh
 */
public class GraphicalGame {

    private Scene scene;
    private int numberOfPlayers = 0;
    private MapPane mapPane;
    private CardSet cardSet = new CardSet();
    private File map;
    private File card;
    private Text roundNumber;
    private Text[] scores = new Text[4];
    private Group currentPlayer = new Group();
    private Text diceShow;
    private ImageView dice = new ImageView(new Image(GraphicalGame.class.getResource("resources/icon/dice.png").toExternalForm()));
    private PlayerGroup[] players = new PlayerGroup[4];
    private Pane bottomMenu;
    private BorderPane root;
    private Pane mainBottomMenu;
    private String[] names = new String[4];

    public Text getRoundNumber() {
        return roundNumber;
    }

    public String[] getNames() {
        return names;
    }

    public Text[] getScores() {
        return scores;
    }

    public Text getDiceShow() {
        return diceShow;
    }

    public Group getCurrentPlayer() {
        return currentPlayer;
    }

    public BorderPane getRoot() {
        return root;
    }

    public Pane getBottomMenu() {
        return bottomMenu;
    }

    public PlayerGroup getPlayer(int i) {
        return players[i];
    }

    public CardSet getCardSet() {
        return cardSet;
    }

    public MapPane getMapPane() {
        return mapPane;
    }

    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public Board getBoard() {
        return mapPane.getBoard();
    }

    public GraphicalGame(File map, File card, String... name) {
        this.map = map;
        this.card = card;
        numberOfPlayers = name.length;
        for (int i = 0; i < numberOfPlayers; i++) {
            players[i] = new PlayerGroup("graphics/resources/player/" + (i + 1) + ".png");
        }

        for (int i = 0; i < name.length; i++) {
            names[i] = name[i];
        }
    }

    public Scene getScene(Stage primaryStage) {
        root = new BorderPane();
        mainBottomMenu = bottomMenu = new Pane();
        bottomMenu.setPrefHeight(10);

        bottomMenu.setStyle("-fx-background-color: #9b9b9b;");
        bottomMenu.setManaged(false);
        bottomMenu.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent arg0) {
                bottomMenu.setPrefHeight(200);
            }
        });

        bottomMenu.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent arg0) {
                bottomMenu.setPrefHeight(10);
            }
        });

//        bottomMenu.setOrientation(Orientation.HORIZONTAL);

        TilePane mainPane = new TilePane(Orientation.HORIZONTAL);
        bottomMenu.getChildren().add(mainPane);

        TilePane scorePane = new TilePane(Orientation.VERTICAL);
        for (int i = 0; i < numberOfPlayers; i++) {
            scores[i] = new Text(names[i] + ":" + 0);
            scorePane.getChildren().add(scores[i]);
            scores[i].setFont(new Font(20));
        }
        TilePane dicePane = new TilePane(Orientation.VERTICAL);
        dice.setVisible(true);
        dicePane.getChildren().add(dice);
        diceShow = new Text("0 0");
        dicePane.getChildren().add(diceShow);

        currentPlayer.getChildren().add(new PlayerGroup("graphics/resources/player/" + (1) + ".png"));
        mainPane.setHgap(20);

        roundNumber = new Text("Round 1");
        roundNumber.setFont(new Font(20));


        mainPane.getChildren().add(scorePane);
        mainPane.getChildren().add(dicePane);
        mainPane.getChildren().add(currentPlayer);
        mainPane.getChildren().add(roundNumber);

        scene = new Scene(root, 400, 400);
        primaryStage.setFullScreen(true);

        mapPane = new MapPane();
        mapPane.editMode(false);
        try {
            MapIO.LoadMapPane(mapPane, map.getAbsolutePath());
            CardIO.loadSetCard(cardSet, card.getAbsolutePath());
            Board board = mapPane.getBoard();
            for (int i = 0; i < numberOfPlayers; i++) {
                mapPane.add(players[i]);

                players[i].move(board.getBlueCells()[i].getX(), board.getBlueCells()[i].getY());
            }

            primaryStage.setIconified(false);
            primaryStage.setOnHidden(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent arg0) {
                }
            });
            final Stage myStage = primaryStage;

            scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent arg0) {
                    if (arg0.getCode() == KeyCode.F10) {
                                myStage.setFullScreen(true);
                    }
                    if (arg0.getCode() == KeyCode.C) {
                        DicePool.cheatMode = !DicePool.cheatMode;
                        System.out.println("Cheat Mode : " + DicePool.cheatMode + " (press F10 to return to fullscreen");
                    }
                }
            });
        } catch (Exception e) {
            System.out.println("Age oomad biroon yani file naghsha ro nadari :D!");
        }
        root.setCenter(mapPane);
        root.setBottom(bottomMenu);
        return scene;
    }
}
