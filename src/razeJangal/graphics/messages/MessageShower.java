package razeJangal.graphics.messages;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import razeJangal.graphics.GraphicalGame;

/**
 *
 * @author M.a.b.shemirani
 */
public class MessageShower {
    public static void show(Window w, String s)
    {
        final Stage stage = new Stage();
        stage.initOwner(w);
//        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initModality(Modality.NONE);
        BorderPane root = new BorderPane();
        final Scene scene = new Scene(root, 400, 200);
        root.setStyle("-fx-background-color: #9c77b3;");
        
        Text t = new Text(s);
        t.setFont(new Font(40));
        root.setCenter(t);
        
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                stage.close();
            }
        });
        stage.setScene(scene);
        stage.show();
       
    }
    
    public static void show (GraphicalGame gg, String s)
    {
        Thread t = new Thread(new ShowMessageTask(gg, s));
        t.setDaemon(true);
        t.start();
    }
    
}

class ShowMessageTask extends Task
{
    GraphicalGame gg;
    String s;

    public ShowMessageTask(GraphicalGame gg, String s) {
        this.gg = gg;
        this.s = s;
    }
    
    
    @Override
    protected Object call() throws Exception {
        final Pane previous = gg.getBottomMenu();
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                TilePane newPane = new TilePane();
                Text text = new Text(s);
                newPane.setStyle(previous.getStyle());
                text.setFont(new Font(40));
                newPane.getChildren().add(text);
                newPane.setPrefHeight(100);
                gg.getRoot().setBottom(newPane);
            }
        });
        
        Thread.sleep(3000);
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                gg.getRoot().setBottom(gg.getBottomMenu());
                gg.getRoot().getCenter().autosize();
            }
        });
        return this;
    }
    
}