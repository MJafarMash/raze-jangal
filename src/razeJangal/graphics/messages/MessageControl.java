package razeJangal.graphics.messages;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

/**
 *
 * @author user
 */
public class MessageControl {
    private static Group lastRoot;
    private static Group lastMenu;
    public MessageControl() {
        
    }
    
    private void removeLast()
    {
        lastRoot.getChildren().remove(lastMenu);
    }
    
    public static void inputMenu(final MessageControlListener mControl, Group root, String inputs)
    {
        MessageControl.inputMenu(mControl, root, Color.YELLOWGREEN, inputs);
    }
    public static void inputMenu(final MessageControlListener mControl, Group root, Color color, String... inputs)
    {
        Group menu = new Group();
        lastRoot = root;
        lastMenu = menu;
        
        TilePane tilePane = new TilePane(Orientation.VERTICAL);
        tilePane.setPrefColumns(1);
        tilePane.setPrefWidth(400);
        tilePane.setPrefHeight(500);
        tilePane.setVgap(10);

        Rectangle background = new Rectangle(-10, -5, 120, (new Button("sdf").getPrefHeight() + 35) * inputs.length);
        background.setArcHeight(15);
        background.setArcWidth(15);
        background.setFill(color);
        menu.getChildren().add(background);
        menu.getChildren().add(tilePane);

        
        for (int i = 0; i < inputs.length; i++)
        {
            String input = inputs[i];
            Button b = new Button(input);
            b.setPrefWidth(100);
            
            
            tilePane.getChildren().add(b);
            final int selectedItem = i;
            b.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    mControl.select(selectedItem);
                }
            });
        }

//        rectangle.setWidth
        
        menu.setOpacity(0);
        root.getChildren().add(menu);
        FadeTransition fadeTransition = new FadeTransition(Duration.millis(4000));
        fadeTransition.setFromValue(0);
        fadeTransition.setToValue(1);
        fadeTransition.setNode(menu);
        
        fadeTransition.play();
    }
}
