package razeJangal.graphics.messages;

/**
 * 
 * @author M.a.b.shemirani
 */
public interface MessageControlListener {
    void select(int i);
}
