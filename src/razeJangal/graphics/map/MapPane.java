package razeJangal.graphics.map;

import java.io.File;
import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.shape.Line;
import razeJangal.game.board.Board;
import razeJangal.game.board.Connection;
import razeJangal.game.board.DieListener;
import razeJangal.game.board.cell.*;
import razeJangal.graphics.card.CardPlaceGroup;


/**
 * pane that contains Map and Cells, and we can add players inside it.
 * @author M.a.b.shemirani
 */
public class MapPane extends ScrollPane {
    /** content of Pane */
    private Group contentGroup = new Group();
    /** background of map */
    final private ImageView backgroundImageView = new ImageView();
    /** keeps the address of background */
    private String backgroundFilename;
    /** array list of BoardCells */
    final private ArrayList<BoardCell> cells = new ArrayList<>();
    /** array list of Connections */
    final private ArrayList<Connection> connections = new ArrayList<>();
    /** place the cards are in that place */
    final private CardPlaceGroup cardPlaceGroup = new CardPlaceGroup();
    /** because this pane is used inside of both editor and game<br>
     * this field shows that map is in <code>Edit Mode</code>or<br>
     * not.
     */
    private BoardCell cellWhoWantToCreateNewConnection = null;
    /** show that what type of cell must be create when you click on the screen */
    private BoardCellPen pen = null;
    
    public void setBoardCellPen(BoardCellPen pen)
    {
        this.pen = pen;
    }
    public BoardCellPen getBoardCellPen()
    {
        return pen;
    }
    
    public void setCellWhoWantToCreateNewConnection(BoardCell cell)
    {
        cellWhoWantToCreateNewConnection = cell;
    }
    
    public BoardCell getCellWhoWantToCreateNewConnection()
    {
        return cellWhoWantToCreateNewConnection;
    }
    
    private boolean isEditMode = true;
    /**
     * <h5> Constructor </h5> set content of pane.
     */
   
    public MapPane()
    {
        setStyle("-fx-background-color: #c4f848");
        setContent(contentGroup);        
        init();
        setBackgroundDraggable();
        setKeyboardArrow();
        setAddable();
        setPannable(true);
    }
    /**
     * initialize the Map Pane
     */
    private void init()
    {
        add(backgroundImageView);
        add(cardPlaceGroup);
        
        backgroundImageView.setLayoutX(0);
        backgroundImageView.setLayoutY(0);
    }
    /**
     * 
     * @return card place of the map pane
     */
    public CardPlaceGroup getCardPlaceGroup()
    {
        return cardPlaceGroup;
    }
    /**
     * 
     * @return image of background ImageView
     */
    public String getBackgroundFilename()
    {
        return backgroundFilename;
    }
    /**
     * set background of map
     * @param filename absolute address of background.
     */
    public void setBackgroundImage(String filename)
    {
        try
        {
            backgroundImageView.setImage(new Image(new File(filename).toURI().toString()));
            backgroundImageView.setCache(true);
            backgroundFilename = filename;
        }
        catch(Exception e)
        {
        }
        
    }
    /**
     * 
     * @return the board of map  
     */
    public Board getBoard()
    {
        Board board;
        BoardCell[] boardCells = new BoardCell[cells.size()];
        Connection[] connection = new Connection[connections.size()];
                
        for (int i = 0; i < cells.size(); i++)
        {
            cells.get(i).setNumber(i);
            boardCells[i] = cells.get(i);
        }
        for (int i = 0; i < connections.size(); i++)
            connection[i] = connections.get(i);
        
        
        board = new Board(boardCells, connection);
        
        return board;
    }
    /**
     * set the board of map
     * @param board 
     */
    public void setBoard(Board board)
    {
        cells.clear();
        connections.clear();
        contentGroup.getChildren().clear();
        init();
        BoardCellPen.reset();

        for (int i = 0; i < board.getBoardCells().length; i++)
        {
            cells.add(board.getBoardCells()[i]);
            //giving soul to cell
            BoardCellGroup boardCellGroup = new BoardCellGroup(cells.get(i));
            //it's for test
            BoardCellPen.updateLimitness(BoardCellPen.getCellType(cells.get(i)), true);
            add(boardCellGroup);
        }
        for (int i = 0; i < board.getConnections().length; i++)
        {
            connections.add(board.getConnections()[i]);
            //giving soul to connection
            ConnectionGroup connectionGroup = new ConnectionGroup(connections.get(i));
            add(connectionGroup);
        }
        
    }
    /**
     * it is used for adding a new board cell when map is in edit mode
     * @param boardCell 
     */
    public void add(BoardCell boardCell)
    {
        if (isEditMode)
        {
            BoardCellGroup boardCellGroup = new BoardCellGroup(boardCell);
            add(boardCellGroup);
            cells.add(boardCell);
        }
    }
    /**
     * it is used for adding a new connection when map is in edit mode
     * @param connection 
     */
    public void add(Connection connection)
    {
        if (isEditMode)
        {
            ConnectionGroup connectionGroup = new ConnectionGroup(connection);
            add(connectionGroup);
            connections.add(connection);
        }
    }
    /**
     * add node to the group that is inside of MapPane
     * @param node 
     */
    final public void add(Node node)
    {
        contentGroup.getChildren().add(node);
        if (node instanceof BoardCellGroup)
            ((BoardCellGroup)node).setMap(this);
        if (node instanceof ConnectionGroup)
            ((ConnectionGroup)node).setMap(this);
        if (node instanceof CardPlaceGroup)
            ((CardPlaceGroup)node).setMap(this);
    }
    final public void remove(Node node)
    {
        contentGroup.getChildren().remove(node);
    }
    /**
     * 
     * @param editMode if is true, map can be edit else map is not is edit mode
     */
    public void editMode(boolean editMode)
    {
        isEditMode = editMode;
    }
    /**
     * 
     * @return that map is in edit mode or not
     */
    public boolean isEditMode()
    {
        return isEditMode;
    }
    /**
     * 
     * @return <code> array list </code> of cells of the map
     */
    public ArrayList<BoardCell> getCells()
    {
        return cells;
    }
    /**
     * 
     * @return <code> array list </code> of connections of the map
     */
    public ArrayList<Connection> getConnections()
    {
        return connections;
    }
        
    /**
     * 
     * @return group that keeps the content of pane
     */
    public Group getContentGroup()
    {
        return contentGroup;
    }

    /**
     * set map to form that we can change add cell too map.
     */
    private void setAddable() {
        final MapPane mapPane = this;
        
        setOnMouseClicked(new EventHandler<MouseEvent>() {
            

            @Override
            public void handle(MouseEvent event) {
                if (mapPane.isEditMode() && !event.isAltDown() && !event.isControlDown() && event.getClickCount() >= 2)
                {
                    if (pen != null && !pen.getText().equals("0"))
                    {
                        BoardCell cell;
                        if (pen.getBoardCell().isBlueCell())
                            cell = new BlueCell(cells.size());
                        else if (pen.getBoardCell().isOrangeCell())
                            cell = new OrangeCell(cells.size());
                        else if (pen.getBoardCell().isVioletCell())
                            cell = new VioletCell(cells.size());
                        else if (pen.getBoardCell().isRedCell())
                            cell = new RedCell(cells.size());
                        else
                            cell = new GreenCell(cells.size());
                        System.out.println(getHvalue());
                        cell.move(event.getX() + getHvalue() * (getContentGroup().getBoundsInLocal().getWidth() - getWidth()), event.getY() + getVvalue() * (getContentGroup().getBoundsInLocal().getHeight() - getHeight()));
                        add(cell);
                        BoardCellPen.updateLimitness(pen, true);
                    }
                }
            }
        });
    }
    private void setKeyboardArrow()
    {
        setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.RIGHT)
                    setHvalue(getHvalue() + 0.05);

                if (event.getCode() == KeyCode.LEFT)
                    setHvalue(getHvalue() - 0.05);
                if (event.getCode() == KeyCode.UP)
                    setVvalue(getVvalue() - 0.05);
                if (event.getCode() == KeyCode.DOWN)
                    setVvalue(getVvalue() + 0.05);
            }
        });
    }
    private void setBackgroundDraggable() {
        final MapPane mapPane = this;
        setOnDragOver(new EventHandler<DragEvent>() {

            @Override
            public void handle(DragEvent event) {
                if (event.getGestureSource() != mapPane &&(event.getDragboard().hasImage() || event.getDragboard().hasFiles()))
                {
                    event.acceptTransferModes(TransferMode.COPY);
                }
                
                event.consume();
              
            }
        });
        
        setOnDragDropped(new EventHandler<DragEvent>() {

            @Override
            public void handle(DragEvent event) {
                boolean success = false;
                Dragboard db = event.getDragboard();
                if (isEditMode)
                {
                    if (db.hasImage())
                    {
                        success = true;
                        backgroundImageView.setImage(db.getImage());
                    }
                    else if (db.hasFiles())
                    {
                        setBackgroundImage(db.getFiles().get(0).getAbsolutePath());
                        if (backgroundImageView.getImage() != null)
                            success = true; // i know that it have a bug! when background before drag is not null, but drag is not successfulli it have bug

                    }
                    event.setDropCompleted(success);

                    event.consume();
                }
            }
        });
    }
}
/**
 * Connection group is graphical connection
 * @author M.a.b.shemirani
 */
class ConnectionGroup extends Group implements DieListener
{
    private MapPane mapPane;
    private Line connectionLine = new Line(0, 0, 0, 0);
    private Connection connection;

    public ConnectionGroup(Connection connection) {
        this.connection = connection;
        connectionLine.setStrokeWidth(5);
        connectionLine.setEffect(new GaussianBlur());
        connectionLine.setCache(true);
        
        setLine();
        setRemovable();
        
        connection.addDieListener(this);
        connection.getEndCell().addListener(new BoardCellListener() {

            @Override
            public void moveCell() {
                setLine();
            }

            @Override
            public void highlight() {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public void dehighlight() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        });
        connection.getStartCell().addListener(new BoardCellListener() {

            @Override
            public void moveCell() {
                setLine();
            }

            @Override
            public void highlight() {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public void dehighlight() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        });

        
        getChildren().add(connectionLine);
    }
    /**
     * set the start/end of line with the cells position
     */
    private void setLine()
    {
        setLayoutX(connection.getStartCell().getX());
        setLayoutY(connection.getStartCell().getY());
        double startX, startY, endX, endY, ratio, distance;
        
        distance = Math.sqrt(Math.pow((connection.getEndCell().getY() - connection.getStartCell().getY()), 2) +
                Math.pow((connection.getEndCell().getX() - connection.getStartCell().getX()), 2));
   
        ratio = (BoardCellGroup.CELL_RADIUS + 4) / distance;
        startX = (connection.getEndCell().getX() - connection.getStartCell().getX()) * ratio;
        startY = (connection.getEndCell().getY() - connection.getStartCell().getY()) * ratio;
        endX = (connection.getEndCell().getX() - connection.getStartCell().getX()) * (1 - ratio);
        endY = (connection.getEndCell().getY() - connection.getStartCell().getY()) * (1 - ratio);

        connectionLine.setStartX(startX);
        connectionLine.setStartY(startY);
        connectionLine.setEndX(endX);
        connectionLine.setEndY(endY);
    }
    
    public void setMap(MapPane mapPane)
    {
        this.mapPane = mapPane;
    }
    private void setRemovable()
    {
        final ConnectionGroup connectionGroup = this;
        setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if (mapPane.isEditMode())
                {
                    if (event.isControlDown())
                    {
                        mapPane.getConnections().remove(connection);
                        mapPane.getContentGroup().getChildren().remove(connectionGroup);
                    }
                }
            }
        });
    }
}
// chera vaghti listener ha ro ta'rif kardam nagoft bayad final bashan???????
// TODO in tike ke ham too listener tekrar shode ham too connection group bad smell e ba'dan dorosesh mikonam ye method basash!