package razeJangal.graphics.map;

import java.util.ArrayList;
import javafx.animation.FillTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.StrokeTransition;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.effect.*;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.StrokeType;
import javafx.util.Duration;
import razeJangal.game.board.Connection;
import razeJangal.game.board.cell.BoardCell;
import razeJangal.game.board.cell.BoardCellListener;
import razeJangal.graphics.map.ConnectionGroup;
import razeJangal.graphics.map.MapPane;
import razeJangal.graphics.server.GraphicalServer;
import razeJangal.graphics.server.waitAndNotify.Notify;

/**
 * board cell group is graphical Cell
 * @author M.a.b.shemirani
 */
class BoardCellGroup extends Group
{
    protected MapPane mapPane;
    static final double CELL_RADIUS = 25;
    private BoardCell boardCell;
    private ConnectionGroup newConnectionGroup;
    
    /**
     * for highlight
     */
    private boolean isPlay = false;
    private ParallelTransition parallelTransition = new ParallelTransition();
    private Circle circle;
    private Circle highlightCircle;
    // <for dragging>
    private double mouseX;
    private double mouseY;
    private double lastX;
    private double lastY;
    // </for dragging>
    
    public BoardCell getBoardCell()
    {
        return boardCell;
    }
    public BoardCellGroup(BoardCell boardcell)
    {
        this.boardCell = boardcell;
        final BoardCell cell = boardCell;
        final BoardCellGroup cellGroup = this;
        setLayoutX(cell.getX());
        setLayoutY(cell.getY());

        drawCircle();
       
        boardCell.addListener(new BoardCellListener() {

            @Override
            public void moveCell() {       
                setLayoutX(cell.getX());
                setLayoutY(cell.getY());
            }
            
            @Override
            public void highlight() {
                cellGroup.highlight();
            }
            
            @Override
            public void dehighlight() {
                cellGroup.dehighlight();
            }
            
            
        });
        
        setDraggable();
        setRemovable();
    }
    
    public void highlight()
    {
        highlight(Color.GOLD);
    }
    
    public void highlight(Color color)
    {
        FillTransition fillTransition = new FillTransition(Duration.millis(1000), highlightCircle, Color.rgb((int)Color.GOLD.getRed(), (int)Color.GOLD.getGreen(), (int)Color.GOLD.getBlue(), 0.00), Color.rgb((int)Color.GOLD.getRed(), (int)Color.GOLD.getGreen(), (int)Color.GOLD.getBlue(), 0.50));
        fillTransition.setAutoReverse(true);
        fillTransition.setCycleCount(Timeline.INDEFINITE);
        
        StrokeTransition strokeTransition = new StrokeTransition(Duration.millis(1000), circle, Color.BLACK, color);
        strokeTransition.setAutoReverse(true);
        strokeTransition.setCycleCount(Timeline.INDEFINITE);

        if (!isPlay)
        {
            parallelTransition = new ParallelTransition();
            parallelTransition.getChildren().addAll(
                    fillTransition,
                    strokeTransition
            );
            isPlay = true;
            parallelTransition.play();
        }
    }
    public void dehighlight(Color color)
    {
        if (parallelTransition != null)
        {
            parallelTransition.pause();
            parallelTransition.getChildren().clear();
            parallelTransition = new ParallelTransition();
        }
        if (isPlay)
        {
 
            isPlay = false;
            FillTransition fillTransition = new FillTransition(Duration.millis(1000), highlightCircle, Color.rgb((int)Color.GOLD.getRed(), (int)Color.GOLD.getGreen(), (int)Color.GOLD.getBlue(), 0.50), Color.rgb((int)Color.GOLD.getRed(), (int)Color.GOLD.getBlue(), (int)Color.GOLD.getGreen(), 0.00));
            fillTransition.setAutoReverse(false);
            fillTransition.setCycleCount(1);

            StrokeTransition strokeTransition = new StrokeTransition(Duration.millis(1000), circle, color, Color.BLACK);
            strokeTransition.setAutoReverse(false);
            strokeTransition.setCycleCount(1);

            parallelTransition.getChildren().addAll(
                    fillTransition,
                    strokeTransition
            );

            parallelTransition.play();
        }
    }
    public void dehighlight()
    {
        dehighlight(Color.GOLD);
    }
    protected void setRemovable()
    {
        final BoardCellGroup boardCellGroup = this;
        setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) { 
                if (mapPane.isEditMode())
                {

                    if (event.isControlDown())
                    {
                        mapPane.getContentGroup().getChildren().remove(boardCellGroup);
                        mapPane.getCells().remove(boardCell);
                        ArrayList<Connection> mustBeRemove = new ArrayList<>();
                        for (Connection connection : mapPane.getConnections())
                            if( connection.getStartCell().getNumber() == boardCell.getNumber() ||
                                    connection.getEndCell().getNumber() == boardCell.getNumber())
                                mustBeRemove.add(connection);

                        for (Connection connection : mustBeRemove)
                        {
                            mapPane.getConnections().remove(connection);
                            if (connection.getDieListener() instanceof ConnectionGroup)
                                mapPane.getContentGroup().getChildren().remove((ConnectionGroup)connection.getDieListener());
                        }
                        
                        BoardCellPen.updateLimitness(BoardCellPen.getCellType(boardCell), false);
                        
                    }
                }
            }
        });
    }
    private void drawCircle()
    {
        
        
        circle = new Circle(0, 0, CELL_RADIUS);
        circle.setStroke(Color.BLACK);
        circle.setStrokeType(StrokeType.OUTSIDE);
        circle.setStrokeWidth(5);
        circle.setEffect(new BoxBlur(5, 5, 5));
        
        highlightCircle = new Circle(0, 0, CELL_RADIUS);
        highlightCircle.setFill(Color.rgb((int)Color.GOLD.getRed(), (int)Color.GOLD.getGreen(), (int)Color.GOLD.getBlue(), 0.00));
        highlightCircle.setBlendMode(BlendMode.OVERLAY);

        if (boardCell.isBlueCell())
            circle.setFill(Color.BLUE);
        else if(boardCell.isOrangeCell())
            circle.setFill(Color.ORANGE);
        else if (boardCell.isRedCell())
            circle.setFill(Color.RED);
        else if (boardCell.isVioletCell())
            circle.setFill(Color.VIOLET);
        else
            circle.setFill(Color.GREEN);
 
        getChildren().add(circle);
        getChildren().add(highlightCircle);
        setCache(true);
    }
    
    protected void setDraggable()
    {
        setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                mouseX = event.getSceneX();
                mouseY = event.getSceneY();
                
                lastX = getLayoutX();
                lastY = getLayoutY();

                if (isPlay)
                {
                    GraphicalServer.choice = boardCell.getNumber();
                    Thread thread = new Thread(new Notify(GraphicalServer.Gaurd));
                    thread.setDaemon(true);
                    thread.start();
                }
            }
        });
        
        setOnMouseDragReleased(new EventHandler<MouseDragEvent>() {

            @Override
            public void handle(MouseDragEvent event) {
                if (event.isAltDown() && mapPane.getCellWhoWantToCreateNewConnection() != null)
                {
                    if (boardCell.getNumber() != mapPane.getCellWhoWantToCreateNewConnection().getNumber())
                    {
                        Connection newConnection = new Connection(boardCell, mapPane.getCellWhoWantToCreateNewConnection());
                        mapPane.add(newConnection);
                    }
                }
                
                mapPane.setCellWhoWantToCreateNewConnection(null);
                mapPane.remove(newConnectionGroup);
            }
        });
        
        setOnMouseReleased(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                mapPane.setCellWhoWantToCreateNewConnection(null);
                mapPane.remove(newConnectionGroup);
            }
        });

        setOnDragDetected(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if (event.isAltDown())
                    startFullDrag();

            }
        });
        setOnMouseDragged(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                lastX += event.getSceneX() - mouseX;
                lastY += event.getSceneY() - mouseY;
                
                if (mapPane.isEditMode() && !event.isAltDown())
                {
                    if (lastX > BoardCellGroup.CELL_RADIUS + 8 && lastY > BoardCellGroup.CELL_RADIUS + 8)
                        boardCell.move(lastX, lastY);
                }
                
                if (mapPane.isEditMode() && event.isAltDown())
                {
                    mapPane.remove(newConnectionGroup);
                    BoardCell tempBoardStartCell = new BoardCell();
                    BoardCell tempBoardEndCell = new BoardCell();
                    tempBoardStartCell.move(getLayoutX(), getLayoutY());
                    tempBoardEndCell.move(lastX, lastY);
                    Connection tempConnection = new Connection(tempBoardStartCell, tempBoardEndCell);
                    
                    newConnectionGroup = new ConnectionGroup(tempConnection);
                    newConnectionGroup.setMap(mapPane);
                    mapPane.setCellWhoWantToCreateNewConnection(boardCell);
                    mapPane.add(newConnectionGroup);
                }
                else
                    mapPane.remove(newConnectionGroup);

                mouseX = event.getSceneX();
                mouseY = event.getSceneY();
            }
        });
    }
    public void setMap(MapPane mapPane)
    {
        this.mapPane = mapPane;
    }
}
