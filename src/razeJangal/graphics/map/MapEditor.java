package razeJangal.graphics.map;

import java.io.File;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.stage.*;
import razeJangal.game.board.cell.*;
import razeJangal.graphics.map.MapIO;
import razeJangal.graphics.map.MapPane;

/**
 *
 * @author M.a.b.shemirani
 */
public class MapEditor extends Application{

    private MapPane mapPane;
    private BoardCellPen[] pens = new BoardCellPen[5];
    
    public static void main(String[] args)
    {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        
        primaryStage.setTitle("Map Editor");
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());
        
        primaryStage.setScene(getScene(primaryStage));
        primaryStage.show();
    }
    public Scene getScene(Stage primaryStage)
    {
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, primaryStage.getMaxWidth(), primaryStage.getMaxHeight() -50);
        
        
        TilePane menu = new TilePane(Orientation.VERTICAL);
        menu.setPrefColumns(2);
        BoardCellPen redPen = pens[0] =  new BoardCellPen(0, 0, new RedCell(-1));
        BoardCellPen greenPen = pens[1] =  new BoardCellPen(0, 0, new GreenCell(-1));
        BoardCellPen violetPen = pens[2] =  new BoardCellPen(0, 0, new VioletCell(-1));
        BoardCellPen orangePen = pens[3] =  new BoardCellPen(0, 0, new OrangeCell(-1));
        BoardCellPen bluePen = pens[4]  = new BoardCellPen(0, 0, new BlueCell(-1));
        
        menu.getChildren().add(greenPen);
        menu.getChildren().add(orangePen);
        menu.getChildren().add(violetPen);
        menu.getChildren().add(redPen);
        menu.getChildren().add(bluePen);

        menu.setStyle("-fx-background-color: #a7dc28;");

        Button loadDefaultBackgroundButton = createLoadDefaultBackgroundButton();
        Button loadButton = createLoadButton();
        Button saveButton = createSaveButton();
        Button helpButton = createHelpButton(primaryStage);
        
        
        menu.getChildren().add(helpButton);
//        menu.getChildren().add(loadDefaultBackgroundButton); //UNDER CONSTRUCTION
        menu.getChildren().add(loadButton);
        menu.getChildren().add(saveButton);

        mapPane = new MapPane();
        redPen.setMap(mapPane);
        greenPen.setMap(mapPane);
        violetPen.setMap(mapPane);
        orangePen.setMap(mapPane);
        bluePen.setMap(mapPane);
        
        root.setCenter(mapPane);
        root.setLeft(menu);
        
        return scene;
    }
    
    private Button createLoadButton()
    {
        ImageView loadIcon = new ImageView(new Image(MapEditor.class.getResource("../resources/icon/load.png").toExternalForm()));
        Button loadButton = new Button("", loadIcon);
        
        loadButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                FileChooser fileChooser = new FileChooser();
//                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("map"));//UNDER CONSTRUCTION
                File loadFile = fileChooser.showOpenDialog(null);
                MapIO.LoadMapPane(mapPane, loadFile.getAbsolutePath());
            }
        });
        return loadButton;
    }
    
    // UNDER CONSTRUCTION!!!
    private Button createLoadDefaultBackgroundButton()
    {
        ImageView imageIcon = new ImageView(new Image(MapEditor.class.getResource("../resources/icon/image.png").toExternalForm()));
        Button imageButton = new Button("", imageIcon);
        
        imageButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                String defaultBackgroundPath = "def_background.jpg";
                mapPane.setBackgroundImage(defaultBackgroundPath);
                System.out.println(mapPane.getBackgroundFilename());
            }
        });
        return imageButton;
 
    }

    private Button createSaveButton()
    {
        ImageView saveIcon = new ImageView(new Image(MapEditor.class.getResource("../resources/icon/save.png").toExternalForm()));
        Button saveButton = new Button("", saveIcon);
        
        saveButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                boolean canSave = true;

                for (int i = 0; i < 5; i++)
                    if (!(pens[i].getText().equals("0") || pens[i].getText().equals("∞")))
                        canSave = false;
                
                if (canSave)
                {
                    FileChooser fileChooser = new FileChooser();
                    File saveFile = fileChooser.showSaveDialog(null);
                    MapIO.saveMapPane(mapPane, saveFile.getAbsolutePath());
                }
            }
        });
        
        return saveButton;

    }

    private Button createHelpButton(Stage primaryStage)
    {
        ImageView helpIcon = new ImageView(new Image(MapEditor.class.getResource("../resources/icon/help.png").toExternalForm()));
        Button helpButton = new Button("", helpIcon);


        Popup popup = new Popup();
        popup.getContent().add(helpButton);
        popup.show(primaryStage);
        
        helpButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                
                
                System.out.println("image icon for default background image\n"
                        + "drag an image from your computer and drop in the window to set background to that image\n"
                        + "click one pen and the double click on the scene\n"
                        + "hold alt + drag an cell to another for creating connection\n"
                        + "hold ctrl + click on a connection or cell for delete that");
            }
        });

        return helpButton;

    }
}
