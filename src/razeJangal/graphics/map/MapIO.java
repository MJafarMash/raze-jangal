package razeJangal.graphics.map;

import java.io.*;
import java.util.Formatter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import razeJangal.game.board.BoardIO;

/**
 * class for saving map of game
 * @author M.a.b.shemirani
 */
public class MapIO {
    /**
     * save mapPane in file, first save board in a .map file<br>
     * and next copy background image with #boardFileName_background.*<br>
     * in the same folder.
     * @param mapPane
     * @param filename destination of map file
     */
    public static void saveMapPane(MapPane mapPane, String filename) 
    {
        BoardIO.SaveBoard(mapPane.getBoard(), filename);
        if (mapPane.getBackgroundFilename() != null)
            copyFile(mapPane.getBackgroundFilename(), filename.split("[/.]", 2)[0] + "_background." + mapPane.getBackgroundFilename().split("[/.]", 2)[1]);
        saveMapDetails(mapPane, filename);
    }
    
    private static void saveMapDetails(MapPane mapPane, String filename)
    {
        try {
            Formatter formatter = new Formatter(new File(filename.split("[/.]", 2)[0] + "_details.det"));
            //card Place
            formatter.format("%s %f %f\n", "CardPlace", mapPane.getCardPlaceGroup().getLayoutX(), mapPane.getCardPlaceGroup().getLayoutY());
            formatter.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MapIO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private static void loadMapDetails(MapPane mapPane, String filename)
    {
        try {
            Scanner scanner = new Scanner(new File(filename.split("[/.]", 2)[0] + "_details.det"));
        
            while (scanner.hasNextLine())
            {
                String oneLineDetailFromScanner = scanner.nextLine();
                String[] seperatedDate = oneLineDetailFromScanner.split("[ ]");
                switch (seperatedDate[0])
                {
                    case "CardPlace":
                        double newX = Double.parseDouble(seperatedDate[1]);
                        double newY = Double.parseDouble(seperatedDate[2]);
                        mapPane.getCardPlaceGroup().move(newX, newY);
                        break;
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MapIO.class.getName()).log(Level.SEVERE, null, ex);
        }
   
    }
//    private static void 
    /**
     * copy a file with path same as sourceFile to a file with path destinationFile
     * @param sourceFile
     * @param destinationFile 
     */
    public static void copyFile(String sourceFile, String destinationFile)
    {
        if (sourceFile.endsWith(destinationFile))
            return;
        System.out.println(sourceFile + "_" + destinationFile);

        try {
            File f1 = new File(sourceFile);
            File f2 = new File(destinationFile);
            InputStream in = new FileInputStream(f1);

            OutputStream out = new FileOutputStream(f2);
            
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        } catch (IOException iOException) {
        }
    }
    /**
     * Load a map from filename and set mapPane to it.
     * @param mapPane
     * @param filename 
     */
    public static void LoadMapPane(MapPane mapPane, String filename)
    {
        mapPane.setBoard(BoardIO.LoadBoard(filename));
        String backgroundPath = filename.split("[/.]", 2)[0] + "_background.";

        //supported extension for background
        final String[] SUPPORTED_EXTENSTIONS = {"jpg", "png", "bmp", "gif"};
        
        for (String ext : SUPPORTED_EXTENSTIONS)
            if (new File(backgroundPath + ext).exists())
            {
                backgroundPath = backgroundPath + ext;
                break;
            }
        
        File background = new File(backgroundPath);
        if (background.exists())
        {
            mapPane.setBackgroundImage(background.getAbsolutePath());
        }
        
        loadMapDetails(mapPane, filename);
    }
}
//TODO map ro dar jar file berizam!
//TODO map editor undo , redo dashte bashe