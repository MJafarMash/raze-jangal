package razeJangal.graphics.map;

import razeJangal.graphics.map.BoardCellGroup;
import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import razeJangal.game.board.cell.BoardCell;

/**
 *
 * this class is used in map editor for selecting a map 
 * @author M.a.b.shemirani
 */
public class BoardCellPen extends BoardCellGroup{
    private Text text = new Text();
    private int type = 0;
    private static ArrayList<BoardCellPen> allOfPens = new ArrayList<>();
    
    final private static int[] MAX ={
    4, //MAX_BLUE = 4;
    1, //MAX_RED = 1;
    13, //MAX_ORANGE = 13;
    1, //MAX_VIOLET = 1;
    -1 //MAX_GREEN = -1;
    };
    
    private static int[] no ={
    0, //NO_BLUE = 0;
    0, //NO_RED = 0;
    0, //NO_ORANGE = 0;
    0, //NO_VIOLET = 0;
    0 //NO_GREEN = 0;
    };
    public BoardCellPen(double x, double y,BoardCell boardCell)
    {
        super (boardCell);
        setLayoutX(x);
        setLayoutY(y);
        
        type = getCellType(boardCell);
        draw();
        setSelectable();
        allOfPens.add(this);
    }

    public static int getCellType(BoardCell boardCell)
    {
        int type;
        if (boardCell.isBlueCell())
            type = 0;
        else if (boardCell.isRedCell())
            type = 1;
        else if (boardCell.isOrangeCell())
            type = 2;
        else if (boardCell.isVioletCell())
            type = 3;
        else
            type = 4;
        return type;
    }
    
    private void draw()
    {
        getChildren().add(text);
        text.setLayoutX(-CELL_RADIUS + 2);
        text.setLayoutY(15);
        text.setFont(new Font("tahoma", 40));
        setText();
    }
    
    private void setText()
    {
        String sText = "∞";
        if (MAX[type] - no[type] >= 0)
            sText = (MAX[type] - no[type]) + "";
        text.setText(sText);
    }
    public static void reset()
    {
        
        for (int i = 0; i < 5; i++)
            no[i] = 0;

        for (BoardCellPen boardCellPen : allOfPens)
                boardCellPen.setText();
        
    }
    public static void updateLimitness(int type, boolean isAdded)
    {
        int delta = 1;
        if (!isAdded)
            delta = -1;
        
        no[type] += delta;

        for (BoardCellPen boardCellPen : allOfPens)
            if (boardCellPen.getType() == type)
                boardCellPen.setText();
    }
    
    public static void updateLimitness(BoardCellPen pen, boolean isAdded)
    {
//        int delta = 1;
//        if (!isAdded)
//            delta = -1;
//        
//        no[pen.type] += delta;
//        System.out.println("SDFSD");
//        pen.setText();
        updateLimitness(pen.type, isAdded);
    }
    private void setSelectable()
    {
        final BoardCellPen thisPen = this;
        
        text.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if (MAX[type] > no[type] || MAX[type] < 0)
                {
                    if (mapPane.getBoardCellPen() != null)
                        mapPane.getBoardCellPen().dehighlight(Color.CHOCOLATE);
                    
                    thisPen.highlight(Color.CHOCOLATE);
                    mapPane.setBoardCellPen(thisPen);
                }
                event.consume();
            }
        });
        setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if (MAX[type] > no[type] || MAX[type] < 0)
                {
                    if (mapPane.getBoardCellPen() != null)
                        mapPane.getBoardCellPen().dehighlight(Color.CHOCOLATE);
                    
                    thisPen.highlight(Color.CHOCOLATE);
                    mapPane.setBoardCellPen(thisPen);
                }

                event.consume();
            }
        });
    }
    @Override
    protected void setDraggable()
    {
    }
    @Override
    protected void setRemovable()
    {
    }
    
    public int getType()
    {
        return type;
    }
    
    public String getText()
    {
        return text.getText();
    }
}
