package razeJangal.graphics.player;

import java.util.ArrayList;
import javafx.animation.ParallelTransition;
import javafx.animation.PathTransition;
import javafx.animation.RotateTransition;
import javafx.scene.Group;
import javafx.scene.effect.BoxBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.util.Duration;

/**
 * this class is a graphical player that returns a Group which contains some Nodes that<br>
 * gathered together in a group
 * @author M.a.b.shemirani
 */
public class PlayerGroup extends Group{
    /** keeps the base group of player */
    private Group playerGroup;
    private EyeNode[] eyes = new EyeNode[2];
    /** determine pattern of body of player, that is a image address */
    private String bodyPattern;
    /** to design a player, this arrays keeps the accessories, it's not used now */
    private ArrayList<Accessory> accessories = new ArrayList<>();
    
            
    /** keeps the position of player*/
    private double xPos = 50;
    /** keeps the position of player*/
    private double yPos = 250;
    
    /**
     * because body shape is circle , every body radius is bodyRadius
     */
    private static int bodyRadius = 25;
    /**
     * Vertical Distance
     * to create the head region with white color, i draw a circle with vDistance upper than <br>
     * the body circle, and with hRadius(head circle radius).
     */
    private static int vDistance = 15;
    /**
     * Head Circle Radius
     * to create the head region with white color, i draw a circle with vDistance upper than <br>
     * the body circle, and with hRadius(head circle radius).
     */
    private static int hRadius = 35;
    /**
     * Keeps the address of default body pattern, for use at the first constructor.
     */
    private static String defaultBodyPatternFilename = "background2.bmp";
    /**
     * Keeps the address of default eye, for use at the first constructor
     */
    private static String defaultEyeFilename = "graphics/resources/player/eye.png";
    
    
    public void setxPos(double xPos)
    {
        this.xPos = xPos;
        playerGroup.setLayoutX(xPos);
    }
    public void setyPos(double yPos)
    {
        this.yPos = yPos;
        playerGroup.setLayoutY(yPos);
    }
    
    public double getxPos()
    {
        return xPos;
    }
    
    public double getyPos()
    {
        return yPos;
    }
    /**
     * constructor that creates the main Group of the Player Class
     */
    public PlayerGroup(String bodyPatternFilename)
    {
        playerGroup = new Group();
        playerGroup.setLayoutX(xPos);
        playerGroup.setLayoutY(yPos);
//       
            
        creatingBodyAndHead(bodyPatternFilename);
        // start of adding Eyes to group
        eyes[0] = new EyeNode(-17, -17, defaultEyeFilename);
        eyes[1] = new EyeNode(-2, -17, defaultEyeFilename);
        Group eyeGroup = new Group();
        for (EyeNode eye : eyes)
        {
            eye.setX(0 + eye.relativeX);
            eye.setY(0 + eye.relativeY);
            eyeGroup.getChildren().add(eye);
        }
        playerGroup.getChildren().add(eyeGroup);
        // end of adding Eyes to group

//        playerGroup.setLayoutX(xPos);
//        playerGroup.setLayoutY(yPos);
    }
    
    
    private void creatingBodyAndHead(String bodyPatternFilename)
    {
        // start of creating body's circle
        Circle bodyCircle = new Circle(0, 0, bodyRadius, Color.web("white", 0.3));
        bodyCircle.setStrokeType(StrokeType.OUTSIDE);
        bodyCircle.setStroke(new Color(0.0, 0.0, 0.0, 1.0));
        bodyCircle.setEffect(new BoxBlur(2, 2, 3));
        // end of creating body's circle
        
        // start of creating head circle
        Circle headCircle = new Circle(0, 0-bodyRadius-vDistance, hRadius, Color.web("white"));
        final Circle headCircleLayerMask = new Circle(0, 0, bodyRadius);
        headCircle.setClip(headCircleLayerMask);
        // end of creating head circle
        
        // start of creating body image
        bodyPattern = bodyPatternFilename;
        System.out.println("body patttttttern: " + bodyPatternFilename);
        System.out.println(razeJangal.RazeJangal.class.getResource(bodyPatternFilename));
        ImageView bodyImage = new ImageView(
                new Image(
                    razeJangal.RazeJangal.class.getResource(bodyPatternFilename).toExternalForm()
                )
            );
        bodyImage.setCache(true);
        bodyImage.setSmooth(false);
        bodyImage.setX(0 - bodyRadius);
        bodyImage.setY(0 - bodyRadius);
        final Circle bodyImageLayerMask = new Circle(0, 0, bodyRadius);
        bodyImage.setClip(bodyImageLayerMask);
        // end of creating body image
        
        // start of adding Nodes to group
        Circle backgroundCircle = new Circle(0, 0, bodyRadius, Color.web("white"));
        playerGroup.getChildren().addAll(new Group(backgroundCircle, bodyCircle), bodyImage, headCircle);
        getChildren().addAll(playerGroup);
        // end of adding Nodes to group
    }
    /**
     * adding an accessory to a player
     * @param relativeX is x component of relative position from center of player
     * @param relativeY is y component of relative position from center of player
     * @param filename  is address of filename of this accessory
     */
    public void addAccessory(int relativeX, int relativeY, String filename)
    {
        Accessory accessory = new Accessory(relativeX, relativeY, filename);
        playerGroup.getChildren().add(accessory);
        accessories.add(accessory);
    }
    
    public void move(double destinationX, double destinationY)
    {
        Path path = new Path();
        
        playerGroup.setTranslateX(0);
        playerGroup.setTranslateY(0);
        path.getElements().add(new MoveTo(xPos, yPos));
        path.getElements().add(new CubicCurveTo ((destinationX + xPos) / 2, (destinationY + yPos) / 2, (destinationX + xPos) / 2, (destinationY + yPos) / 2, destinationX, destinationY));
        
        PathTransition pathTransition = new PathTransition();
        pathTransition.setDuration(Duration.millis(3000));
        pathTransition.setAutoReverse(false);
        pathTransition.setNode(this);
        pathTransition.setPath(path);
 
        RotateTransition rotateTransition = 
            new RotateTransition(Duration.millis(3000));
        rotateTransition.setNode(this);
        rotateTransition.setAutoReverse(false);
        rotateTransition.setFromAngle(0);
        rotateTransition.setToAngle(360);
      
        ParallelTransition parallelTransition = new ParallelTransition();
        parallelTransition.getChildren().addAll(
                pathTransition,
                rotateTransition
        );
        xPos = destinationX;
        yPos = destinationY;
        parallelTransition.play();
        //        Transition
    }
    //TODO : Move method with animation, that calls setxPos and setyPos
    //TODO : x, y EyeNode, va Accessory bayad nesbat be xPos , yPos bashe
}

/**
 * this class Extends Accessory and this is for Eye!<br>
 * for now it's not different from ImageView but because of next versions can support<br>
 * more complex eyes, (for example too look at some point), i wrote its class.
 * it's different from Accessory because every body most have Eye, but accessory is <br>
 * is not necessary. and because of next version can support more things.
 * @author M.a.b.shemirani
 */
class EyeNode extends Accessory
{
    EyeNode(int relativeX, int relativeY, String filename)
    {
        super(relativeX, relativeY, filename);
    }
         
}

/**
 * this class Extends ImageView and this is for Accessories!<br>
 * for now it's not different from ImageView but because of next versions can support<br>
 * more complex Accessories, (for example rotating), i wrote its class.
 * @author M.a.b.shemirani
 */
class Accessory extends ImageView
{
    int relativeX;
    int relativeY;
    Accessory(int relativeX, int relativeY, String filename)
    {
        super(new Image(razeJangal.RazeJangal.class.getResource(filename).getPath()/*.toExternalForm()*/));
        this.relativeX = relativeX;
        this.relativeY = relativeY;
        setSmooth(false);
        setCache(true);
    }
}