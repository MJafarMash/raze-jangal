/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package razeJangal.graphics.server.waitAndNotify;

import javafx.concurrent.Task;

/**
 *
 * @author user
 */

abstract public class  Wait extends Task
{
    private Object o;
    
    public Wait(Object o)
    {
        this.o = o;
    }
    @Override
    protected Object call() throws Exception {
        synchronized(o)
        {
            o.wait();
        }
        codeAfterWait();
        return this;
    }
    
    abstract public void codeAfterWait();
}
