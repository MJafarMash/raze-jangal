package razeJangal.graphics.server.waitAndNotify;

import javafx.concurrent.Task;

/**
 *
 * @author M.a.b.shemirani
 */
public class Notify extends Task{
    private Object o;
    public Notify(Object o)
    {
        this.o = o;
    }

    @Override
    protected Object call() throws Exception {
        synchronized(o)
        {
            o.notifyAll();
        }
        return this;
    }
}
