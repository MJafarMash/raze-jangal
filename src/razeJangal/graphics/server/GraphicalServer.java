/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package razeJangal.graphics.server;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;
import razeJangal.game.GameRemote;
import razeJangal.game.MoveType;
import razeJangal.game.Player;
import razeJangal.game.board.cell.BoardCell;
import razeJangal.game.board.cell.OrangeCell;
import razeJangal.game.rule.Game;
import razeJangal.graphics.GraphicalGame;
import razeJangal.graphics.card.MiniCardGroup;
import razeJangal.graphics.messages.MessageShower;
import razeJangal.graphics.player.PlayerGroup;
import razeJangal.graphics.server.waitAndNotify.Notify;

/**
 *
 * @author M.a.b.shemirani
 */
public class GraphicalServer implements GameRemote{
    private Game game;
    private Stage primaryStage;
    private GraphicalGame graphicalGame;
    public static Object Gaurd = new Object();
    public static int choice;
   
    public GraphicalServer(Stage primaryStage, GraphicalGame graphicGame) {
        this.primaryStage = primaryStage;
        this.graphicalGame = graphicGame;
    }

    @Override
    public void playerTurn_ShowFirstMove() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                MessageShower.show(graphicalGame, "Dice Number Are :" + game.getDicePool().getFirstDiceFace() + " "+ game.getDicePool().getSecondDiceFace());
                graphicalGame.getDiceShow().setText(game.getDicePool().getFirstDiceFace() + " " + game.getDicePool().getSecondDiceFace());
                for (BoardCell cell : game.getBoard().getBoardCells())
                    cell.dehighlight();
                for (BoardCell cell : game.getPosibleChoices(game.getCurrentPlayerCell(), game.getDicePool().getFirstDiceFace()))
                    cell.highlight();
                
                for (BoardCell cell : game.getPosibleChoices(game.getCurrentPlayerCell(), game.getDicePool().getSecondDiceFace()))
                    cell.highlight();


            }
        });
    }

    @Override
    public void playerTurn_ShowSecondMove() {
        BoardCell here = game.getCurrentPlayerCell();
        int unusedFace = game.getDicePool().getFirstDiceFace();
        if (game.getDicePool().isUsedFirstDice())
            unusedFace = game.getDicePool().getSecondDiceFace();

        final BoardCell[] possibleCells = game.getPosibleChoices(here, unusedFace);
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                for (BoardCell cell : game.getBoard().getBoardCells())
                    cell.dehighlight();
                
                for (BoardCell cell : possibleCells)
                    cell.highlight();
            }
        });
        
    }

    @Override
    public void playerTurn_Double() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                TilePane menu = new TilePane(Orientation.VERTICAL);
                graphicalGame.getRoot().setRight(menu);
                Button orange = new Button("Go To Orange");
                Button changeTreasure = new Button("Change Treasure");
                Button goToViolet = new Button("Go To Violet");

                menu.getChildren().add(orange);
                menu.getChildren().add(changeTreasure);
                menu.getChildren().add(goToViolet);
                
                menu.setStyle(graphicalGame.getBottomMenu().getStyle());
                
                orange.setOnAction(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent arg0) {
                        graphicalGame.getRoot().setRight(null);
                        graphicalGame.getRoot().getCenter().autosize();
                        
                        game.getDicePool().useBothDice();
                        
                        for (BoardCell cell : game.getBoard().getBoardCells())
                            cell.dehighlight();

                        for (BoardCell cell : game.getPossibleOrangeChoices())
                            cell.highlight();
                        
                    }
                });
                
                changeTreasure.setOnAction(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent arg0) {
                        graphicalGame.getRoot().setRight(null);
                        graphicalGame.getRoot().getCenter().autosize();
                      
                        game.getDicePool().useBothDice();

                        choice = -1;
                        
                        Thread thread = new Thread(new Notify(Gaurd));
                        thread.setDaemon(true);
                        thread.start();
                    }
                });
    
                goToViolet.setOnAction(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent arg0) {
                        graphicalGame.getRoot().setRight(null);
                        graphicalGame.getRoot().getCenter().autosize();
                      
                        game.getDicePool().useBothDice();

                        choice = game.getBoard().getVioletCell().getNumber();
                        
                        Thread thread = new Thread(new Notify(Gaurd));
                        thread.setDaemon(true);
                        thread.start();
                    }
                });

            }
        });
    }

    @Override
    public int playerTurn_Input(MoveType moveType) {
        
        try {
            synchronized (Gaurd)
            {
                Gaurd.wait();
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(GraphicalServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                graphicalGame.getRoot().setRight(null);
                graphicalGame.getRoot().getCenter().autosize();
            }
        });
        if (choice == -1)
        {
            game.getDicePool().useBothDice();
            return -1;
        }
        for (BoardCell cell : game.getPosibleChoices(game.getCurrentPlayerCell(), game.getDicePool().getFirstDiceFace()))
            if (cell.getNumber() == choice)
                game.getDicePool().useFirstDice();
        if (!game.getDicePool().isUsedFirstDice())
            game.getDicePool().useSecondDice();
        return choice;
    }

    
    @Override
    public void inOrangeCell_Show(final OrangeCell cell) {

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                MiniCardGroup miniTreasure = graphicalGame.getCardSet().getCard(cell.getTreasure().getName()).getMiniCardGroup();
                graphicalGame.getMapPane().add(miniTreasure);
                miniTreasure.setLayoutY(cell.getY() + 55);
                miniTreasure.setLayoutX(cell.getX());
                
                miniTreasure.setShowAndHide();
            }
        });
    }

    @Override
    public void inRedCell_Show() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                TilePane menu = new TilePane(Orientation.VERTICAL);
                graphicalGame.getRoot().setRight(menu);
                
                menu.setStyle(graphicalGame.getBottomMenu().getStyle());

                Button idont = new Button("I don't Know");
                menu.getChildren().add(idont);
                
                idont.setOnAction(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent arg0) {
                        graphicalGame.getRoot().setRight(null);
                        graphicalGame.getRoot().getCenter().autosize();
                        choice = -1;

                        Thread thread = new Thread(new Notify(Gaurd));
                        thread.setDaemon(true);
                        thread.start();
                    }
                });
                
                for (BoardCell cell : game.getBoard().getBoardCells())
                    cell.dehighlight();
                
                for (BoardCell cell : game.getBoard().getOrangeCells())
                    cell.highlight();
            }
        });
    }

    @Override
    public int inRedCell_Input() {
        try {
            synchronized (Gaurd)
            {
                Gaurd.wait();
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(GraphicalServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                graphicalGame.getRoot().setRight(null);
                graphicalGame.getRoot().getCenter().autosize();
            }
        });
        
        int result = 0;
        for (int i = 0; i < 13; i++)
            if (game.getBoard().getOrangeCells()[i].getNumber() == choice)
                return i;
        return -1;
    }

    @Override
    public void inBlueCell_Show(final Player p) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                MessageShower.show(graphicalGame, "Player Kicked");

                //                graphicalGame.getPlayer(p.getNumber()).move(game.getBoard().getBlueCells()[p.getNumber()].getX(), game.getBoard().getBlueCells()[p.getNumber()].getY());
            }
        });
    }

    @Override
    public void achieveTreasure() {

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                for(int i = 0; i < graphicalGame.getNumberOfPlayers(); i++)
                    graphicalGame.getScores()[i].setText(graphicalGame.getNames()[i] + ":" + game.getPlayers()[i].getScore());
                MessageShower.show(graphicalGame, "Player Achieved Treasure");
            }
        });
    }

    @Override
    public void roundStart_Show() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(GraphicalServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                System.out.println("THIS ROUND TREASURE : " + game.getCurrentTreasure().getName());
                graphicalGame.getMapPane().getCardPlaceGroup().getChildren().remove(graphicalGame.getCardSet().getCard(game.getCurrentTreasure().getName()));
                graphicalGame.getMapPane().getCardPlaceGroup().getChildren().add(graphicalGame.getCardSet().getCard(game.getCurrentTreasure().getName()));
                graphicalGame.getRoundNumber().setText("Round " + game.getRound());
                MessageShower.show(graphicalGame, "Round " + game.getRound() + " Started");
            }
        });
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(GraphicalServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void treasureChange_Show() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                primaryStage.show();
                MessageShower.show(graphicalGame, "Round's Treasure Changed");
                graphicalGame.getMapPane().getCardPlaceGroup().getChildren().add(graphicalGame.getCardSet().getCard(game.getCurrentTreasure().getName()));
            }
        });
    }

    @Override
    public void playerMove(final int i, final BoardCell destinationCell) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                graphicalGame.getPlayer(i).move(destinationCell.getX(), destinationCell.getY());
            }
        });
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public void play() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                graphicalGame.getCurrentPlayer().getChildren().clear();
                graphicalGame.getCurrentPlayer().getChildren().add(new PlayerGroup("graphics/resources/player/" + /*(game.getTurn()+1)*/ 1 +".png"));
            }
        });
        game.play(this);
    }
    
}
