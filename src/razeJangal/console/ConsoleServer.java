package razeJangal.console;

import java.util.Scanner;

import razeJangal.game.GameRemote;
import razeJangal.game.MoveType;
import razeJangal.game.Player;
import razeJangal.game.board.cell.BoardCell;
import razeJangal.game.board.cell.OrangeCell;
import razeJangal.game.rule.Game;

/**
 * implement {@link razeJangal.game.GameRemote GameRemote}
 * @author Mohammad ali baghershemirani
 *
 */
public class ConsoleServer implements GameRemote
{
	Game game;
	
	ConsoleServer(Game game)
	{
		this.game = game;
		ConsoleDialogs.setGame(game);
	}
	
	ConsoleServer()
	{
		
	}
	
	public void setGame(Game game)
	{
		this.game = game;
		ConsoleDialogs.setGame(game);
	}
	
	@Override
	public void playerTurn_ShowFirstMove()
	{
		ConsoleDialogs.showPositions();
		ConsoleDialogs.showDicePool();
		ConsoleDialogs.showFirstDiceFirstLine();
		int choiceNumber = 1;
		BoardCell here = game.getCurrentPlayerCell();
		
		BoardCell[] possibleCells = game.getPosibleChoices(here, 
					game.getDicePool().getFirstDiceFace());
		
		int nFirstDiceChoice = possibleCells.length;
		for (int i = 0; i < nFirstDiceChoice; i++)
			ConsoleDialogs.showIthLineFirstDice(choiceNumber++, possibleCells[i],
					game.getDicePool().getSecondDiceFace());

		if (game.getDicePool().isDouble())
			return;
		possibleCells = game.getPosibleChoices(here, 
				game.getDicePool().getSecondDiceFace());

		int nSecondDiceChoice = possibleCells.length;
		for (int i = 0; i < nSecondDiceChoice; i++)
			ConsoleDialogs.showIthLineFirstDice(choiceNumber++, possibleCells[i],
					game.getDicePool().getFirstDiceFace());
	}

	@Override
	public void playerTurn_ShowSecondMove()
	{
		ConsoleDialogs.showSecondDiceFirstLine();
		int choiceNumber = 1;
		BoardCell here = game.getCurrentPlayerCell();

		int unusedFace = game.getDicePool().getFirstDiceFace();
		if (game.getDicePool().isUsedFirstDice())
			unusedFace = game.getDicePool().getSecondDiceFace();
		
		BoardCell[] possibleCells = game.getPosibleChoices(here, unusedFace);
		int nChoices = possibleCells.length;
		
		for (int i = 0; i < nChoices; i++)
			ConsoleDialogs.showIthLineSecondDice(choiceNumber++,
					possibleCells[i]);
	}

	@Override
	public void playerTurn_Double()
	{
		BoardCell here = game.getCurrentPlayerCell();
		int nFirstDiceChoice = game.getPosibleChoices(here, game.getDicePool()
				.getFirstDiceFace()).length;
		
		int choiceNumber = nFirstDiceChoice + 1;
		for (OrangeCell cell:game.getPossibleOrangeChoices())
			ConsoleDialogs.showIthLineSecondDice(choiceNumber++, cell);
		ConsoleDialogs.showIthLineSecondDice(choiceNumber++, game.getBoard().getVioletCell());
		ConsoleDialogs.showChangeTreasureChoice(choiceNumber++);
		
	}

	@Override
	public int playerTurn_Input(MoveType moveType)
	{
		Scanner input = new Scanner(System.in);
		int choice = input.nextInt();
		if (moveType == MoveType.FIRST_DICE || moveType == MoveType.DOUBLE_DICE)
		{
			BoardCell origin = game.getCurrentPlayerCell();
			BoardCell[] firstDiceChoices = game.getPosibleChoices(origin, game.getDicePool().getFirstDiceFace());
			if (choice <= firstDiceChoices.length)
			{
				game.getDicePool().useFirstDice();
				return firstDiceChoices[choice - 1].getNumber();
			}
			choice -= firstDiceChoices.length;
			
			if (moveType == MoveType.FIRST_DICE)
			{
				BoardCell[] secondDiceChoices = game.getPosibleChoices(origin, game.getDicePool().getSecondDiceFace());
				if (choice <= secondDiceChoices.length)
				{
					game.getDicePool().useSecondDice();
					return secondDiceChoices[choice - 1].getNumber();
				}
			}
			else if (moveType == MoveType.DOUBLE_DICE)
			{
				game.getDicePool().useBothDice();
				BoardCell[] orangeDiceChoices = game.getPossibleOrangeChoices();
				if (choice <= orangeDiceChoices.length)
					return orangeDiceChoices[choice - 1].getNumber();
				
				if (choice == orangeDiceChoices.length + 1)
					return game.getBoard().getVioletCell().getNumber();
				return -1;// treasure change selected
			}
		}
		else
		{
			BoardCell[] choices;
			BoardCell origin = game.getCurrentPlayerCell();

			if (game.getDicePool().isUsedFirstDice())
				choices = game.getPosibleChoices(origin, game.getDicePool().getSecondDiceFace());
			else
				choices = game.getPosibleChoices(origin, game.getDicePool().getFirstDiceFace());
			
			return choices[choice - 1].getNumber();
		}
		return 0;
	}

	@Override
	public void inOrangeCell_Show(OrangeCell cell)
	{
		ConsoleDialogs.showTreasure(cell, game.getPlayers()[game.getTurn()]);
	}

	@Override
	public void inRedCell_Show()
	{
		ConsoleDialogs.showRedCellFirstLine();
		ConsoleDialogs.showRedCellIdonKnowChoice();
		for (int i = 0; i < game.getBoard().getOrangeCells().length; i++)
			ConsoleDialogs.showRedCellChoices(i + 2, game.getBoard()
					.getOrangeCells()[i].getNumber());
	}

	@Override
	public int inRedCell_Input()
	{
		Scanner input = new Scanner(System.in);
		int choice = input.nextInt();
		if (choice == 1)
			return -1;
		return choice - 2;
	}

	@Override
	public void inBlueCell_Show(Player p)
	{
		ConsoleDialogs.showMoveToBlueCell(p);
	}

	@Override
	public void achieveTreasure()
	{

		ConsoleDialogs.showAchievementOfTreasure();
	}

	@Override
	public void roundStart_Show()
	{
		ConsoleDialogs.showRoundStart();
	}

	@Override
	public void treasureChange_Show()
	{
		ConsoleDialogs.showTreasureChange();		
	}
	
	public void play()
	{
		game.play(this);
	}

    @Override
    public void playerMove(int i, BoardCell destinationCell) {
    }

}
