package razeJangal.console;

import razeJangal.game.DefaultMap;
import razeJangal.game.board.Board;
import razeJangal.game.board.BoardLoader;
import razeJangal.game.rule.Game;
import java.util.Scanner;

/**
 * run the console servers
 * @author Mohammad ali baghershemirani
 *
 */
public class ConsoleRunner
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.println("How many players are going to play?");
		int nPlayer = input.nextInt();
		Board board;
		if (args == null || args.length == 0)
			board = new Board(DefaultMap.getCells(), DefaultMap.getConnections());
		else // we want to use another file
		{
			String fileName = "";
			if (args[0].equals("filename:"))
			{
				for (int i = 1; i < args.length; i++) // concat the file name: for example "...\document and setting\..." doucument is on arg and is one arg and setting is one arg
				{
					fileName += args[i];
					if (i < args.length - 1)
						fileName += " ";
				}
			}
			BoardLoader loadMap = new BoardLoader(fileName);
			board = new Board(loadMap.getCells(), loadMap.getConnections());
		}
		ConsoleServer consoleServer = new ConsoleServer();
		Game game = new Game(board, nPlayer);
		consoleServer.setGame(game);
		game.startGame(consoleServer);
		while (game.getRound() < 14)
			consoleServer.play();
	}

}
