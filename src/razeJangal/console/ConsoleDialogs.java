package razeJangal.console;

import razeJangal.game.Player;
import razeJangal.game.board.cell.BoardCell;
import razeJangal.game.board.cell.OrangeCell;
import razeJangal.game.rule.Game;

/**
 * class of dialogs that most show in console
 * @author Mohammad ali baghershemirani
 *
 */
public class ConsoleDialogs
{
	private static final String ROUND_START = "Round number %d started, this round's goal treasure is %s";
	private static final String TREASURE_CHANGE = "Round number %d, this round's goal treasure has changed to %s";
	private static final String POSITIONS_SHOW = "Current Positions:";
	private static final String DICE_POOL_SHOW = "Dice Numbers for Player ";
	private static final String FIRST_DICE_FIRST_LINE = "Player %d's Choices:";
	private static final String TWOSTEP_Ith_CHOICE = "%d. Move to %s, and then move %d cells";
	private static final String SECOND_DICE_FIRST_LINE = "Player %d's Choices(Second Dice):";
	private static final String ONESTEP_Ith_CHOICE = "%d. Move to %s";
	private static final String CHANGE_CURRENT_TREASURE_CHOICE = "%d. Change current round's goal treasure";
	private static final String RED_CELL_FIRST_LINE = "Player %d is in %s, and can attempt to guess goal treasure's place:";
	private static final String I_DON_KNOW_CHOICE = "1. I don't know";
	private static final String Ith_TREASURE_CHOICE = "%d. %s";
	private static final String ACHIEVE_TREASURE = "Player %d has won this round's goal treasure, %s";
	private static final String MOVE_TO_BLUE_CELL = "Player %d is moved to Blue cells.";
	private static final String SEE_TREASURE = "Cell %d's treasure as seen by player %d is %s";
	
	private static Game game;
	
	public static void setGame(Game game)
	{
		ConsoleDialogs.game = game;
	}
	
	public static void showTreasure(OrangeCell cell, Player player)
	{
		System.out.printf(SEE_TREASURE, cell.getNumber(), player.getNumber(), cell.getTreasure().getName());
		System.out.println();
	}
	
	public static void showRoundStart()
	{
		System.out.printf(ROUND_START, game.getRound(), game.getCurrentTreasure());
		System.out.println();
	}
	
	public static void showTreasureChange()
	{
		System.out.printf(TREASURE_CHANGE, game.getRound(), game.getCurrentTreasure());
		System.out.println();
	}
	
	public static void showPositions()
	{
		System.out.print(POSITIONS_SHOW);
		for (int i = 1; i <= game.getPlayerPositions().length; i++)
		{
			int number;
			number = game.getBoard().getCell(game.getPlayerPositions()[i - 1]).getNumber();
			
			if (game.getBoard().getCell(game.getPlayerPositions()[i - 1]).isBlueCell())
				number = 0;
			System.out.print(" " + i + "->" + number);
		}
		System.out.println();
	}
	
	public static void showDicePool()
	{
		System.out.print(DICE_POOL_SHOW);
		System.out.print(game.getPlayers()[game.getTurn()].getNumber() + ": " + game.getDicePool().getFirstDiceFace());
		System.out.print(" " + game.getDicePool().getSecondDiceFace());
		System.out.println();
	}
	
	public static void showFirstDiceFirstLine()
	{
		System.out.printf(FIRST_DICE_FIRST_LINE, game.getPlayers()[game.getTurn()].getNumber());
		System.out.println();
	}
	
	/**
	 * 
	 * @param ithLine :number of choice
	 * @param cell :cell of choice
	 * @param secondDice :next move, dice face
	 */
	public static void showIthLineFirstDice(int ithLine, BoardCell cell,
			int secondDice)
	{
		System.out.printf(TWOSTEP_Ith_CHOICE, ithLine,
				cell.toString(), secondDice);
		System.out.println();
	}
	
	public static void showSecondDiceFirstLine()
	{
		System.out.printf(SECOND_DICE_FIRST_LINE, game.getPlayers()[game.getTurn()].getNumber());
		System.out.println();
	}
	
	/**
	 * 
	 * @param ithLine :number of choice
	 * @param cell :cell of choice
	 */
	public static void showIthLineSecondDice(int ithLine, BoardCell cell)	
	{
		System.out.printf(ONESTEP_Ith_CHOICE, ithLine, cell.toString());
		System.out.println();
	}
	
	public static void showChangeTreasureChoice(int ithLine)
	{
		System.out.printf(CHANGE_CURRENT_TREASURE_CHOICE, ithLine);
		System.out.println();
	}
	
	public static void showRedCellFirstLine()
	{
		System.out.printf(RED_CELL_FIRST_LINE, game.getPlayers()[game.getTurn()].getNumber(), game.getBoard().getRedCell().toString());
		System.out.println();
	}
	
	/**
	 * 
	 * @param ithLine :number of choice
	 * @param selectedCell :the cell that we choosed
	 */
	public static void showRedCellChoices(int ithLine, int selectedCell)
	{
		System.out.printf(Ith_TREASURE_CHOICE, ithLine, selectedCell);
		System.out.println();
	}
	
	public static void showRedCellIdonKnowChoice()
	{
		System.out.println(I_DON_KNOW_CHOICE);
	}
	
	/**
	 * 
	 * @param player that is moved to blue cell
	 */
	public static void showMoveToBlueCell(Player player)
	{
		System.out.printf(MOVE_TO_BLUE_CELL, player.getNumber());
		System.out.println();
	}
	
	public static void showAchievementOfTreasure()
	{
		for (OrangeCell cell:game.getBoard().getOrangeCells())
			if (cell.getTreasure().equals(game.getCurrentTreasure()))
				showTreasure(cell, game.getPlayers()[game.getTurn()]);
		System.out.printf(ACHIEVE_TREASURE, game.getPlayers()[game.getTurn()].getNumber(), game.getCurrentTreasure());
		System.out.println();
	}
}
