package razeJangal.game.rule;

import java.util.ArrayList;
import java.util.Random;
import razeJangal.game.board.*;
import razeJangal.game.board.cell.BoardCell;
import razeJangal.game.board.cell.OrangeCell;
import razeJangal.game.*;

/**
 * game included {@link Board},{@link razeJangal.game.Player Player} and {@link razeJangal.game.DicePool pairs of dice}<br>
 * @author Mohammad ali baghershemirani
 *
 */
public class Game
{
	
	private Player[] players;
	private final DicePool dicePool = new DicePool();
	private Board board;
	/** keep the turn of that a what player now most play*/
	private int turn = 0;
	/** keep that what treasures are achieved*/
	private boolean[] isAchievedTreasures = new boolean[13];
	/** keep the <code>index</code> treasure */
	private int nCurrentTreasure; // number of current treasure
	/** keep the round number*/
	private int round = 1;
	
	/**
	 * go to next round
	 */
	public void nextRound()
	{
		round++;
	}
	
	/**
	 * 
	 * @return a<code> array of {@link razeJangal.game.Player Player}</code> that are play in game.
	 */
	public Player[] getPlayers()
	{
		return players;
	}
	
	/**
	 * 
	 * @return the dice pool
	 */
	public DicePool getDicePool()
	{
		return dicePool;
	}
	
	/**
	 * 
	 * @return board of game
	 */
	public Board getBoard()
	{
		return board;
	}
	
	/**
	 * 
	 * @return that what player most play now
	 */
	public int getTurn()
	{
		return turn;
	}
	
	/**
	 * 
	 * @return <code>cell</code> that current player is on that
	 */
	public BoardCell getCurrentPlayerCell()
	{
		return board.getCell(players[turn].getPosition());
	}
	
	/**
	 * set achievement of a treasure
	 * @param i index of treasure
	 * @param isAchieved state of achievement
	 */
	void setIsAchievedTreasures(int i, boolean isAchieved)
	{
		isAchievedTreasures[i] = isAchieved;
	}
	
	/**
	 * move player i to position
	 * @param i
	 * @param position
	 */
	void movePlayers(int i, int position)
	{
		players[i].setPosition(position);
	}
	
	/**
	 * <h5>Game Constructor</h5>
	 * initialize the game
	 * @param board of game
	 * @param nPlayer 
	 */
	public Game(Board board, int nPlayer)
	{
		this.board = board;
		
		players = new Player[nPlayer];
		for (int i = 1; i <= nPlayer; i++)
			players[i - 1] = new Player(i);
		
		for (int i = 0; i < nPlayer; i++)
			players[i].setPosition(board.getBlueCells()[i].getNumber());
		setTreasures(distributeTreasures());
		randomSelectTreasure();
	}
	
	/**
	 * show start of game
	 * @param remote
	 */
	public void startGame(GameRemote remote)
	{
		remote.roundStart_Show();
	}
	
	/**
	 * play a step of game
	 * @param remote that implements {@link razeJangal.game.GameRemote GameRemote}
	 */
	public void play(GameRemote remote)
	{
		if (round > 13)
			return;
		Government government = new Government(this, remote);
		dicePool.roll();
		government.play();
		nextTurn();
	}
	
	/**
	 * 
	 * @return an array of empty <code>orange cells</code>
	 */
	public OrangeCell[] getPossibleOrangeChoices()
	{
		ArrayList<OrangeCell> choices = new ArrayList<OrangeCell>();
		for (int i = 0; i < board.getOrangeCells().length; i++)
			if (Government.isCellEmpty(this, board.getOrangeCells()[i]))
				choices.add(board.getOrangeCells()[i]);
		
		OrangeCell[] result = new OrangeCell[choices.size()];
		for (int i = 0; i < result.length; i++)
			result[i] = choices.get(i);

		return result;
	}

	/**
	 * find the cells that we can go there from here with a specific dice 
	 * @param origin :the cell that we are on that cell
	 * @param diceFace face of dice
	 * @return an array of <code>cells</code> that we can go there
	 */
	public BoardCell[] getPosibleChoices(BoardCell origin, int diceFace)
	{
		ArrayList<BoardCell> choices = new ArrayList<BoardCell>();
		//DFS
		Queue queue = new Queue();
		queue.createElement(origin);
		for (int i = 0; i < queue.length(); i++)
		{
			if (queue.getElement(i).length() == diceFace + 1)
			{
				choices.add(queue.getElement(i).getLastCell());
				continue;
			}
			if (queue.getElement(i).length() > diceFace + 1 )
				continue;
			for (BoardCell cell:board.getNeighborCells(queue.getElement(i).getLastCell()))
			{
				if (queue.getElement(i).length() <= 1 || cell.getNumber() != queue.getElement(i).getCell(queue.getElement(i).length() - 2).getNumber())
				{
					if (!cell.isBlueCell()) // is not blue cell
						queue.copyAndAppendElement(queue.getElement(i), cell);
				}
			}
		}
		
		//END OF DFS
		
		
		// remove repeated choices
		for (int i = 0; i < choices.size(); i++)
			for (int j = 0; j < i; j++)
				if (choices.get(i).getNumber() == choices.get(j).getNumber())
				{
					choices.remove(j);
					j--;
					i--;
				}
		
		// convert ArrayList to an Array
		BoardCell[] result = new BoardCell[choices.size()];
		for (int i = 0; i < result.length; i++)
			result[i] = choices.get(i);

		return result;
	}

	/**
	 * set treasure of <b>orange cells</b> of the board
	 * @param treasures
	 */
	private void setTreasures(String[] treasures)
	{
		for (int i = 0; i < treasures.length; i++)
		{
			Treasure t = new Treasure(treasures[i]);
			board.getOrangeCells()[i].setTreasure(t);
		}
	}
	
	/**
	 * select random order of treasures
	 * @return an array with length 13 of <code>Strings</code> that most be set to treasures name
	 */
	private String[] distributeTreasures()
	{
		String[] result = new String[13];
		String[] defaults = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
				"K", "L", "M" };
		
		for (int i = 0; i < defaults.length; i++)
			result[i] = defaults[i];
		
		int repeat = 4; // how many time randomization will occur
		for (int i = 0; i < repeat; i++)
			for (int j = 0; j < result.length; j++)
			{
				// for every index we select another index, and swap them 
				Random random = new Random();
				int secondIndex = random.nextInt(result.length);
				
				//swap
				String temp = result[secondIndex];
				result[secondIndex] = result[j];
				result[j] = temp;
			}
		return result;
	}
	
	/**
	 * go to next turn
	 */
	private void nextTurn()
	{
		turn = (turn + 1) % players.length;
	}

	/**
	 * 
	 * @return <code>treasure</code> of this round
	 */
	public Treasure getCurrentTreasure()
	{
		return board.getOrangeCells()[nCurrentTreasure].getTreasure();
	}
	
	/**
	 * select a treasure that is not achieved
	 */
	public void randomSelectTreasure()
	{
		Random random = new Random();
		int i = random.nextInt(13);
		
		while (isAchievedTreasures[i] == true)
			i = random.nextInt(13);
		
		nCurrentTreasure = i;
	}
	
	/**
	 * 
	 * @return array of <code>positions</code> of players
	 */
	public int[] getPlayerPositions()
	{
		int[] positions = new int[players.length];
		for (int i = 0; i < players.length; i++)
			positions[i] = players[i].getPosition();
		return positions;
	}

	public int getRound()
	{
		return round;
	}
}

/**
 * Queue is used for DFS algorithm
 * <br> this is queue of elements
 * @author Mohammad ali baghershemirani
 *
 */
class Queue
{
	/**
	 * see {@link Element}
	 */
	private ArrayList<Element> elements = new ArrayList<Element>();
	
	/**
	 * create a new element with starting cell, cell
	 * @param cell
	 */
	void createElement(BoardCell cell)
	{
		Element element = new Element(cell);
		elements.add(element);
	}
	
	/**
	 * copy element and add cell to end of the sequence
	 * @param element
	 * @param cell
	 */
	void copyAndAppendElement(Element element, BoardCell cell)
	{
		Element e = new Element();
		for (int i = 0; i < element.length(); i++)
			e.addCell(element.getCell(i));
		e.addCell(cell);
		elements.add(e);
	}
	
	/**
	 * 
	 * @param i
	 * @return i th element of the queue
	 */
	Element getElement(int i)
	{
		return elements.get(i);
	}
	
	/**
	 * 
	 * @return the number of elements
	 */
	int length()
	{
		return elements.size();
	}
	
	public String toString()
	{
		String result = "";
		for (Element e:elements)
			result += e + "\n";
		return result;
	}
}

/**
 * every Element is an sequence of <code>cells</code>
 * @author Mohammad ali baghershemirani
 *
 */
class Element
{
	private ArrayList<BoardCell> cells;
	Element()
	{
		cells = new ArrayList<BoardCell>();
	}
	
	Element(BoardCell cell)
	{
		cells = new ArrayList<BoardCell>();
		cells.add(cell);
	}
	
	/**
	 * add cell to end of the sequence
	 * @param cell
	 */
	void addCell(BoardCell cell)
	{
		cells.add(cell);
	}
	
	/**
	 * 
	 * @param i
	 * @return i th <code>cell</code> of sequence
	 */
	BoardCell getCell(int i)
	{
		return cells.get(i);
	}
	
	/**
	 * 
	 * @return the last <code>cell</code>
	 */
	BoardCell getLastCell()
	{
		return cells.get(cells.size() - 1);
	}
	
	/**
	 * 
	 * @return the number of <code>cells</code> of sequence
	 */
	int length()
	{
		return cells.size();
	}
	
	public String toString()
	{
		String result = "";
		for (BoardCell cell:cells)
			result += " " + cell.getNumber();
		return result;
	}
}
