package razeJangal.game.rule;

import razeJangal.game.GameRemote;
import razeJangal.game.Player;
import razeJangal.game.board.Treasure;
import razeJangal.game.board.cell.BoardCell;
import razeJangal.game.board.cell.OrangeCell;
import razeJangal.game.MoveType;
/**
 * status of the game
 * @author Mohammad ali baghershemirani
 *
 */
enum Status
{
	CONTINUE,
	I_DONT_KNOW,
	RE_RED,
	EXIT;
}

/**
 * govern the rules of game
 * @author Mohammad ali baghershemirani
 *
 */
class Government
{
	private Game game;
	private GameRemote remote;
	
	/**
	 * <h5>Government Constructor</h5>
	 * sets game and remote of government
	 * @param game
	 * @param remote
	 */
	public Government(Game game, GameRemote remote)
	{
		this.game = game;
		this.remote = remote;
	}
	
	/**
	 * plays a step in the game
	 */
	public void play()
	{
		//first move
		MoveType moveType = MoveType.FIRST_DICE;
		remote.playerTurn_ShowFirstMove();//show choices
		if (game.getDicePool().isDouble())//append choices, if it's double dice
		{
			moveType = MoveType.DOUBLE_DICE;
			remote.playerTurn_Double();
		}
		
		int selectedCell = remote.playerTurn_Input(moveType);//selected cell
		
		Status status;
		if (selectedCell == -1) // DiceRool is double and player select the change treasure!
		{
			game.randomSelectTreasure();
			remote.treasureChange_Show();
			status = Status.EXIT;
		}
		else
			status = moveAndCheck(selectedCell); // go to selected cell and do what it should do!	
		
		//if we use abilities of double dice we can not use second move
		if (moveType == MoveType.DOUBLE_DICE && game.getDicePool().isUsedBothDice())
			return;
		
		if (game.getRound() > 13)
			return;
		
		//if guess true, stay in red cell
		status = reRedCheck(status, selectedCell);
		boolean mostPlayAgain = false;
		//if after guessing true select i don't know he can play a new set of moves
		if (status == Status.I_DONT_KNOW)
			mostPlayAgain = true;
		
		//second move
		moveType = MoveType.SECONE_DICE;
		remote.playerTurn_ShowSecondMove();
		selectedCell = remote.playerTurn_Input(moveType);
		status = moveAndCheck(selectedCell);

		if (game.getRound() > 13)
			return;
		//if guess true, stay in red cell
		status = reRedCheck(status, selectedCell);
		//if after guessing true select i don't know he can play a new set of moves
		if (status == Status.I_DONT_KNOW)
			mostPlayAgain = true;
		
		if (mostPlayAgain) // play a new set of moves
		{			
			game.getDicePool().roll();
			play();
		}
		
	}
	
	/**
	 * check if guessed true, again go to red cell
	 * @param status of moves
	 * @param selectedCell stay in red
	 * @return
	 */
	private Status reRedCheck(Status status, int selectedCell)
	{
		//if before he guess true, select i don't know he doesn't gain award
		//and because if after this method, status be I_DONT_KNOW we give him
		//award, if before while status is I_DONT_KNOW, we change it
		if (status == Status.I_DONT_KNOW)
			return Status.CONTINUE;
		
		while (status == Status.RE_RED)
		{
			status = moveAndCheck(selectedCell);
			if (status == Status.I_DONT_KNOW)
				return Status.I_DONT_KNOW;
		}
		return status;
	}
	
	/**
	 * go to selectedCell, and show the options of that cell,<br> 
	 * and check the rules of the game.
	 * @param selectedCell
	 * @return the status of game after move
	 */
	private Status moveAndCheck(int selectedCell)
	{
		//move to selected cell
		game.movePlayers(game.getTurn(), selectedCell);
		remote.playerMove(game.getTurn(), game.getBoard().getCell(selectedCell));
		//check that any player is kicked or not
		kickAnother();
		
		BoardCell currentCell = game.getBoard().getCell(selectedCell);
		if (currentCell.isOrangeCell())
			remote.inOrangeCell_Show((OrangeCell)currentCell);
		
		else if (currentCell.isRedCell())
		{
			remote.inRedCell_Show();
			int selectedOrangeCell = remote.inRedCell_Input();
			
			if (selectedOrangeCell == -1) // "-1" for i don't know!
				return Status.I_DONT_KNOW;
			
			Treasure treasure = game.getBoard().getOrangeCells()[selectedOrangeCell].getTreasure();
			if (game.getCurrentTreasure().equals(treasure)) // guessed true
			{
				game.getPlayers()[game.getTurn()].addScore();
				remote.achieveTreasure();
				game.setIsAchievedTreasures(selectedOrangeCell, true);
				game.nextRound();
				if (game.getRound() < 14)
				{
					game.randomSelectTreasure();
					remote.roundStart_Show();
					return Status.RE_RED;
				}
			}
			else // guessed wrong
			{
				remote.inOrangeCell_Show(game.getBoard().getOrangeCells()[selectedOrangeCell]);
				moveToBlueCell(game.getTurn());
				return Status.CONTINUE;
			}
		} 
		return Status.CONTINUE;
	}
	
	/**
	 * move to blue cell if any player is kicked
	 */
	private void kickAnother()
	{
		for (int i = 0; i < game.getPlayers().length; i++)
			if (game.getPlayers()[i].getNumber() != game.getPlayers()[game.getTurn()].getNumber())
				if (game.getPlayers()[i].getPosition() == game.getPlayers()[game.getTurn()].getPosition())
				{
					moveToBlueCell(i);
					return;
				}	
	}
	
	/**
	 * check that a cell in board of game is empty or not
	 * @param game
	 * @param cell
	 * @return
	 */
	static boolean isCellEmpty(Game game,BoardCell cell)
	{
		for (Player p:game.getPlayers())
			if (p.getPosition() == cell.getNumber())
				return false;
		return true;
	}
	
	/**
	 * show a player is moved to blue cell
	 * @param i
	 */
	private void moveToBlueCell(int i)
	{
		for (BoardCell cell:game.getBoard().getBlueCells())
			if (isCellEmpty(game, cell))
			{
				game.movePlayers(i, cell.getNumber());
                                remote.playerMove(i, game.getBoard().getCell(cell.getNumber()));

                                remote.inBlueCell_Show(game.getPlayers()[i]);
				return;
			}
	}
}
