package razeJangal.game.board;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import razeJangal.game.board.cell.*;

@Deprecated
/**
 * <code>BoardLoader</code> read arrays of {@link #CELLS} and {@link #CONNECTIONS}<br>
 * from file and other classes can use this cells and conncetions.<br>
 * @see razeJangal.game.DefaultMap
 * @author Mohammad ali baghershemirani
 *
 */
public class BoardLoader
{
	/** keep the arrays of cells */
	private BoardCell[] CELLS;
	/** keep the arrays of connections */
	private Connection[] CONNECTIONS ;
	/** keep the path of file */
	private String fileName;
	
	/**
	 * <h5>BoardLoader Constructor</h5>
	 * set path of {@link #fileName}
	 * @param fileName :file's path
	 */
	public BoardLoader(String fileName)
	{
		this.fileName = fileName;
		createMap();
	}
	
	/**
	 * 
	 * @return an <code>array of cells</code>
	 */
	public BoardCell[] getCells()
	{
		return CELLS;
	}
	
	/**
	 * 
	 * @return an <code>array of connections</code>
	 */
	public Connection[] getConnections()
	{
		return CONNECTIONS;
	}
	
	/**
	 * read map from file, and save it on {@link #CELLS} and {@link #CONNECTIONS}
	 */
	private void createMap()
	{
		Scanner map;
		try
		{
			//at first we try the input file path
			File file = new File(fileName);
			
			//if doesn't exists, we try to use default.map 
			//that can be in one of these paths:
			if (!file.exists())
				file = new File("default.map");
			if (!file.exists())
				file = new File(".\\default.map");
			if (!file.exists())
				file = new File(".\\src\\default.map");
			if (!file.exists())
				file = new File(".\\src\\razeJangal\\default.map");
			if (!file.exists())
				file = new File(".\\razeJangal\\default.map");
			if (!file.exists())
				file = new File("razeJangal\\default.map");
			if (!file.exists())
				file = new File("razeJangal/default.map");
			
			map = new Scanner(file);
		} catch (FileNotFoundException e)
		{
			//if neither default map nor filename doesn't
			//exists we have nothing so we most face an
			//exception in the future!! :)
			map = null;
		}

		
		ArrayList<BoardCell> cells = new ArrayList<BoardCell>();
		ArrayList<Connection> connections = new ArrayList<Connection>();
		
		//map file contains of two partition, the first
		//partition must be introducing cells,
		//and next must introducing connections
		
		//isCell show that we are in scope of cells or not
		boolean isCell = false;
		//isConnection show that we are in scope of connections or not
		boolean isConnection = false;
		
		while (map.hasNext())//map is not finished
		{
			//in first of both partitions we have a
			//string that says, a partition started.
			if (!map.hasNextInt())
			{
				String next = map.next();
				
				//we entered the cells partition
				if (next.equals("cells:"))
				{
					isCell = true;
					isConnection = false;
				}
				
				//we entered the connections partition
				else if (next.equals("connections:"))
				{
					//store the cells that we red from file
					//into the CELLS because now, we now the
					//number of cells and we don't have anymore
					//cells
					
					CELLS = new BoardCell[cells.size()];
					for (int i = 0; i < CELLS.length; i++)
						CELLS[i] = cells.get(i);

					isCell = false;
					isConnection = true;
				}
			}
			if (isCell)
			{
				//read with format (Color Id) from file
				int number = map.nextInt();
				String color = map.next();;
	
				if (color.equals("green"))
				{
					GreenCell temp = new GreenCell(number);
					cells.add(temp);
				}
				else if (color.equals("blue"))
				{
					BlueCell temp = new BlueCell(number);
					cells.add(temp);
				}
				else if (color.equals("red"))
				{
					RedCell temp = new RedCell(number);
					cells.add(temp);
				}
				else if (color.equals("violet"))
				{
					VioletCell temp = new VioletCell(number);
					cells.add(temp);
				}
				else if (color.equals("orange"))
				{
					OrangeCell temp = new OrangeCell(number);
					cells.add(temp);
				}

			}
			
			if (isConnection)
			{
				//read with format (firstCellId, SecondCellId)
				BoardCell first = new BoardCell();
				BoardCell second = new BoardCell();
				
				int firstNum = map.nextInt();
				for (BoardCell cell: CELLS)
					if (cell.getNumber() == firstNum)
						first = cell;
				
				int secondNum = map.nextInt();
				for (BoardCell cell: CELLS)
					if (cell.getNumber() == secondNum)
						second = cell;
				
				Connection c = new Connection(first, second);
				connections.add(c);
			}
			
			if (!map.hasNext())
			{
				//after that map is finished, we store
				//connections into the CONNECTIONS
				CONNECTIONS = new Connection[connections.size()];
				for (int i = 0; i < CONNECTIONS.length; i++)
					CONNECTIONS[i] = connections.get(i);

			}
		}

	}
}

