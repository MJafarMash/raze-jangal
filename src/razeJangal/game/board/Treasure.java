package razeJangal.game.board;

/**
 * <code>Treasure</code>the treasure that are behind orange cells
 * @author Mohammad ali baghershemirani
 *
 */
public class Treasure
{
	/** keep the name of the treasure*/
	private String name;
	
	/**
	 * <h5>Treasure Constructor</h5>
	 * @param name that most be name of treasure
	 */
	public Treasure(String name)
	{
		this.setName(name);
	}

	/**
	 * 
	 * @return the name of treasure
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * 
	 * @param treasure that we want to compare with <b>this</b> treasure
	 * @return <code>true</code> if the name of two treasure is same, else returns <code>false</code>
	 */
	public boolean equals(Treasure treasure)
	{
		if (treasure.getName().equals(name))
			return true;
		else
			return false;
	}

	/**
	 * set the name of treasure
	 * @param name that treasure most be set to this name
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
}
