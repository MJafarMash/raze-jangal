package razeJangal.game.board.cell;

/**
 * violet cell of the board is from this type
 * @author Mohammad ali baghershemirani
 *
 */
public class VioletCell extends BoardCell
{
	public VioletCell(int number)
	{
		setNumber(number);
	}
	
	@Override
	public boolean isVioletCell()
	{
		return true;
	}
	
	public String toString()
	{
		return getNumber() + "(Violet)";
	}
}
