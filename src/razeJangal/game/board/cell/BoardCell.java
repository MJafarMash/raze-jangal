package razeJangal.game.board.cell;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * all cell's are inheritenced from this class
 * @see BlueCell
 * @see RedCell
 * @see GreenCell
 * @see OrangeCell
 * @see VioletCell
 * @author Mohammad ali baghershemirani
 * 
 */
public class BoardCell implements Serializable
{
	/** keep the <i>id</i> of the cell */
	private int number;
        /** keep the array of listener for moving cell position*/
        transient private ArrayList<BoardCellListener> listeners = new ArrayList<>();
        
	/** keep neighbors of a cell */
	private BoardCell[] neighbors;
	/** x component of position */
        private double x;
        /** y component of position */
        private double y;
        /**
         * 
         * @return x component of position of this cell
         */
        public double getX()
        {
            return x;
        }
        /**
         * 
         * @return y component of position of this cell
         */
        public double getY()
        {
            return y;
        }
        
        public void highlight()
        {
            ArrayList<BoardCellListener> mostBeRemove = new ArrayList<>();
            for (BoardCellListener listener : listeners)
                try{
                    listener.highlight();
                }
                catch(Exception e)
                {
                    mostBeRemove.add(listener);
                }

            for (BoardCellListener listener : mostBeRemove)
                listeners.remove(listener);

        }

        public void dehighlight()
        {
            ArrayList<BoardCellListener> mostBeRemove = new ArrayList<>();
            for (BoardCellListener listener : listeners)
                try{
                    listener.dehighlight();
                }
                catch(Exception e)
                {
                    mostBeRemove.add(listener);
                }

            for (BoardCellListener listener : mostBeRemove)
                listeners.remove(listener);

        }

        /**
         * set the position of cell
         * @param x component of new position
         * @param y component of new position
         */

        public void move(double x, double y)
        {
            this.x = x;
            this.y = y;
            
            ArrayList<BoardCellListener> mostBeRemove = new ArrayList<>();
            for (BoardCellListener listener : listeners)
                try{
                    listener.moveCell();
                }
                catch(Exception e)
                {
                    mostBeRemove.add(listener);
                }
            
            //TODO be nazar miad vaghti line ro pak mikonam in seda zade nemishe, hey bagher! fek kon chera!
            //TODO fahmidam, chon shey'e pak nashode khob! ama age pak nashode pas ki refrencesh ro dare???
            for (BoardCellListener listener : mostBeRemove)
                listeners.remove(listener);
        }
        /** set number of neighbors of cell */
	public void setNumberOfNeighbors(int n)
	{
		neighbors = new BoardCell[n];
	}
	
	/**
	 * 
	 * @param i th neighbor
	 * @param neighbor that we want to set to neighbors[i]
	 */
	public void setNeighbor(int i, BoardCell neighbor)
	{
		neighbors[i] = neighbor;
	}
	
	/**
	 * 
	 * @return An array of <code>BoardCell</code> that are neighbor of this cell
	 */
	public BoardCell[] getNeighbors()
	{
		return neighbors;
	}
	/**
	 * 
	 * @return <code>true</code>  if this cell is <b>Orange</b>, else returns <code>false</code>
	 */
	public boolean isOrangeCell()
	{
		return false;
	}
	
	/**
	 * 
	 * @return <code>true</code> if this cell is <b>Red</b>, else returns <code>false</code>
	 */
	public boolean isRedCell()
	{
		return false;
	}
	
	/**
	 * 
	 * @return <code>true</code> if this cell is <b>Violet</b>, else returns <code>false</code>
	 */
	public boolean isVioletCell()
	{
		return false;
	}
	
	/**
	 * 
	 * @return <code>true</code> if this cell is <b>Blue</b>, else returns <code>false</code>
	 */
	public boolean isBlueCell()
	{
		return false;
	}
	
	/**
	 * 
	 * @return number of the cell
	 */
	public int getNumber()
	{
		return number;
	}
	
	/**
	 * set number of the cell
	 * @param number must be greater than zero
	 */
	public void setNumber(int number)
	{
		if (number >= 0)
			this.number = number;
	}
	
        /**
         * add listener for changing position of cell
         * @param listener 
         */
        public void addListener(BoardCellListener listener)
        {
            if (listeners == null)
                listeners = new ArrayList<>();
            listeners.add(listener);
        }
        
        public ArrayList<BoardCellListener> getListeners()
        {
            return listeners;
        }
        
        @Override
	public String toString()
	{
		return "" + number;
	}
}
