package razeJangal.game.board.cell;

/**
 * green cells of the board are from this type
 * @author Mohammad ali baghershemirani
 *
 */
public class GreenCell extends BoardCell
{
	public GreenCell()
	{
	}
	
	public GreenCell(int number)
	{
		setNumber(number);
	}
	
}
