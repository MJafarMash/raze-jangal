package razeJangal.game.board.cell;

/**
 * red cell of the board is from this type
 * @author Mohammad ali baghershemirani
 *
 */

public class RedCell extends BoardCell
{
	public RedCell(int number)
	{
		setNumber(number);
	}
	
	@Override
	public boolean isRedCell()
	{
		return true;
	}
	
	public String toString()
	{
		return getNumber() + "(Red)";
	}
}
