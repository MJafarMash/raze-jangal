package razeJangal.game.board.cell;

/**
 * blue cells of the board are from this type
 * @author Mohammad ali baghershemirani
 *
 */
public class BlueCell extends BoardCell
{
	public BlueCell(int number) 
	{
		setNumber(number);
	}

	@Override
	public boolean isBlueCell()
	{
		return true;
	}
	
	/**
	 * because field <code> number </code> is id of the blue cells and <br>
	 * id of cells most be different when we want to print <br>
	 * position of players, we print 0 instead of printing <i>id</i> of <br>
	 * id of the cell.
	 */
	@Override
	public String toString()
	{
		return "0";
	}

}
