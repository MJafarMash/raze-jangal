package razeJangal.game.board.cell;
import razeJangal.game.board.Treasure;

/**
 * orange cells of the board that <br> contain treasures are from this type
 * @author Mohammad ali baghershemirani
 *
 */
public class OrangeCell extends BoardCell
{
	/** keep the <b>treasure</b> that is behind of this cell*/
	private Treasure treasure;

	/**
	 * <h5> OrangeCell Constructor Without Treasure</h5>
	 * in this constructor treasure is not known by cell.<br>
	 * @param number of the cell
	 */
	public OrangeCell(int number)
	{
		setNumber(number);
	}

	/**
	 * 
	 * <h5> OrangeCell Constructor With Treasure</h5>
	 * @param number of the cell
	 * @param treasure that is behind of this cell
	 */
	public OrangeCell(int number, String treasure)
	{
		setNumber(number);
	}

	/**
	 * 
	 * @return treasure of this cell
	 */
	public Treasure getTreasure()
	{
		return treasure;
	}

	/**
	 * set treasure of this cell
	 * @param treasure that will be put in this cell
	 */
	public void setTreasure(Treasure treasure)
	{
		this.treasure = treasure;
	}

	public String toString()
	{
		return getNumber() + "(Orange)";
	}
	@Override
	public boolean isOrangeCell()
	{
		return true;
	}
}
