package razeJangal.game.board.cell;

/**
 * abstract class that when a cell's position changed invoke a method
 * @author M.a.b.shemirani
 */
public interface BoardCellListener {
    abstract public void moveCell();
    public void highlight();
    public void dehighlight();
}
