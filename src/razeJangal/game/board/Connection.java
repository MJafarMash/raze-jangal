package razeJangal.game.board;
import java.io.Serializable;
import razeJangal.game.board.cell.*;
/**
 * 
 * cells that connected, represent by <code>connection</code>
 * @author Mohammad ali baghershemirani
 *
 */

public class Connection implements Serializable
{
	/** By convention we call the cell with smaller number StartCell and etc. */
	private BoardCell StartCell;
	/** By convention we call the cell with greater number EndCell and etc. */
	private BoardCell EndCell;
	
        /** when this connection die, who must know*/
        transient private DieListener listeners;

        
        public Connection(BoardCell StartCell, BoardCell EndCell)
	{
		this.StartCell = StartCell;
		this.EndCell = EndCell;
	}

	/**
	 * 
	 * @return the cell with smaller number
	 */
	public BoardCell getStartCell()
	{
		return StartCell;
	}

	/**
	 * 
	 * @return the cell with greater number
	 */
	public BoardCell getEndCell()
	{
		return EndCell;
	}
        
        public void addDieListener(DieListener dieListener)
        {
            listeners = dieListener;
        }
        
        public DieListener getDieListener()
        {
            return listeners;
        }
}
