package razeJangal.game.board;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * contains methods to read/write board from/to file.
 * @author M.a.b.shemirani
 */
public class BoardIO {
    public static void SaveBoard(Board board, String filename)
    {
        try {
            ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(filename));
            output.writeObject(board);
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Can not write to file!!");
        }
    }
    
    public static Board LoadBoard(String filename)
    {
        try {
            ObjectInputStream input = new ObjectInputStream(new FileInputStream(filename));
            return (Board)input.readObject();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ERROR IN BOARD LOADING!!");
            return null;
        }
    }
}
