package razeJangal.game.board;
import java.io.Serializable;
import java.util.ArrayList;
import razeJangal.game.board.cell.*;

/**
 * <code> Board</code> contains the {@link #cells} and the <br> {@link #connections}
 * (that connect two cells together.)<br>
 * any information about map of game that we need<br>
 * is collected from <code> Board</code>
 * @author Mohammad ali baghershemirani
 *
 */
public class Board implements Serializable
{
	/** keep the array of cells of the game*/
	private BoardCell[] cells;
	/** keep the array of connections of the game*/
	private Connection[] connections;
	/** keep the array of {@link razeJangal.game.board.cell.BlueCell BlueCells} of the board*/
	private BlueCell[] blueCells;
	/** keep the array of {@link razeJangal.game.board.cell.OrangeCell OrangeCells} of the board*/
	private OrangeCell[] orangeCells;
	/** keep the {@link razeJangal.game.board.cell.VioletCell VioletCell} of the board*/
	private VioletCell violetCell;
	/** keep the {@link razeJangal.game.board.cell.RedCell RedCell} of the board*/
	private RedCell redCell;
	
	/**
	 * <h5>Board Constructor</h5>
	 * set the cells and connections with input cells and connections
	 * @param cells
	 * @param connections
	 */
	public Board(BoardCell[] cells, Connection[] connections)
	{
            this.cells = cells;
            this.connections = connections;
	}
	
	/**
	 * <h5>Board Constructor</h5>
	 * set the cells and connections with input board
	 * @param board that we need to copy in this <i>board</i>
	 */
	Board(Board board)
	{
		this(board.cells, board.connections);
	}
	
	/**
	 * 
	 * @return the <code>cell</code> in the board that is <b>red</b>
	 */
	public RedCell getRedCell()
	{
		if (redCell == null)
		{
			for (BoardCell cell: cells)
				if (cell.isRedCell())
					redCell = (RedCell)cell;
		}
		return redCell; 
	}
	
	/**
	 * 
	 * @return the <code>cell</code> in the board that is <b>violet</b>
	 */
	public VioletCell getVioletCell()
	{
		if (violetCell == null)
		{
			for (BoardCell cell: cells)
				if (cell.isVioletCell())
					violetCell =  (VioletCell)cell;
		}
		return violetCell;
	}
	
	/**
	 * 
	 * @return an <code>array of cells</code> in the board that are <b>orange</b>
	 *	<br>if the size of array is not equal to 13 it return
	 * 	<code>null</code><br> because the board is not standard!
	 */
	public OrangeCell[] getOrangeCells()
	{
		if (orangeCells == null)
		{
			OrangeCell[] result = new OrangeCell[13];
			
			int cellFounded = 0; // number of orange cells that has been found!
			
			for (BoardCell cell: cells)
				if (cell.isOrangeCell())
					result[cellFounded++] = (OrangeCell)cell; //add this cell to result.
			
			orangeCells = result;
		}
		
		return orangeCells;
	}

	/**
	 * 
	 * @return an <code> array of cells</code> that are <b>blue</b><br>
	 * returns <code>null</code> if the length of array isn'<br>
	 * same to 4, because in this case board is<br>
	 * not standard
	 */
	public BlueCell[] getBlueCells()
	{
		if (blueCells == null)
		{
		
			BlueCell[] result = new BlueCell[4];
			
			int cellFounded = 0; // number of Blue cells that has been found!
			
			for (BoardCell cell: cells)
				if (cell.isBlueCell())
					result[cellFounded++] = (BlueCell)cell; //add this cell to result.
		
			blueCells = result;
		}
		return blueCells;
	}

	/**
	 * 
	 * @param origin that most this method find its neighbors.
	 * @return an <code>array of cells</code> that are connected to<br>
	 * origin cell with a connection
	 */
	public BoardCell[] getNeighborCells(BoardCell origin)
	{
		//if neighbors are already don't used before, we calculate the neighobrs
		//and keep them into [origin.neighbor], else we use cache
		
		if (origin.getNeighbors() == null)
		{
			//because at the first we don't know number of
			//neighbor cells, we use ArrayList at first
			ArrayList<BoardCell> neighborCells = new ArrayList<>();
			
			//we search on whole connections, if there is a
			//connection that starts or ends with this cell
			//the other site of connection is neighbor of this
			//cell
			for (Connection c: connections)
				if (c.getStartCell().getNumber() == origin.getNumber())
					neighborCells.add(c.getEndCell());
				else if (c.getEndCell().getNumber() == origin.getNumber())
					neighborCells.add(c.getStartCell());
			
			//because working with Array is easier than
			//ArrayList we copy ArrayList into a Array
			
			origin.setNumberOfNeighbors(neighborCells.size());
			for (int i = 0; i < neighborCells.size(); i++)
				origin.setNeighbor(i, neighborCells.get(i));
		}
		return origin.getNeighbors();
	}
	
	/**
	 * 
	 * @param i number of the cell
	 * @return the <code>cell</code> that its number equals <b>i</b>
	 */
	public BoardCell getCell(int i)
	{
		return cells[i];
	}
        
        /**
         * 
         * @return array of game
         */
        public BoardCell[] getBoardCells()
        {
            return cells;
        }
        /**
         * 
         * @return array of connection
         */
        public Connection[] getConnections()
        {
            return connections;
        }
	
}
