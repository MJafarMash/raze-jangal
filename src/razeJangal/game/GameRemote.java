package razeJangal.game;

import razeJangal.game.board.Board;
import razeJangal.game.board.cell.BoardCell;
import razeJangal.game.board.cell.OrangeCell;
import razeJangal.game.rule.Game;

/**
 * interface <b>GameRemote</b> that need for game most be implemented by any runner that want to run game.
 * @author Mohammad ali baghershemirani
 *
 */
public interface GameRemote
{
	/**
	 * show the <b>choices</b> that, player can choose in first move
	 * <br>(<b>not including</b> the choice that are result of this fact that<br>
	 * dices are <b>double</b>)
	 */
	void playerTurn_ShowFirstMove();
	/**
	 * show the <b> choices</b> that, player can choose in second move
	 */
	void playerTurn_ShowSecondMove();
	/**
	 * append the <b>choices</b> that, player can choose in first move,<br>
	 * with choices that are came from <b>double rule</b>.
	 * <br><br>like empty <b>orange cells</b> and <b>violet cell</b> and <b>change treasure choice</b>.
	 */
	void playerTurn_Double();
	/**
	 * input from the desired device a choice from represented choices and returns a
	 * <code>integer</code> that we describe it.
	 * @param moveType that is on these states:(  first move, second move, first move with double face  )
	 * @return <b>if state is first move:</b><br>
	 * returns <code>cell</code> that we most move on it
	 * <br><br> <b>if state is second move:</b><br>
	 * returns <code>cell</code> that we most move on it
	 * <br><br> <b>if state is first move with double face:</b><br>
	 * if we selected an empty orange cell, returns <code>index</code> of that orange cell in the array {@link Game#getPossibleOrangeChoices()}<br>
	 * if we selected the violet cell, return {@link Board#getVioletCell()}
	 * <br>if we want to change treasure returns <b>-1</b>
	 */
	int playerTurn_Input(MoveType moveType);
	/**
	 * when we go on an orange cell this method calls.
	 * @param cell that we are there, to know its treasure
	 */
	void inOrangeCell_Show(OrangeCell cell);
	/**
	 * show the choice that we can choose when we are in red cell<br>
	 * <b>including:</b>
	 * <br>i don't know choice, orange cells
	 */
	void inRedCell_Show();
	/**
	 * get from devices choice
	 * @return the <code>number</code> of cell that we most go after we went to red cell.
	 */
	int inRedCell_Input();
	/**
	 * show that player p goes to blue cell
	 * @param p
	 */
	void inBlueCell_Show(Player p);
	/**
	 * show that a treasure is achieved by a player
	 */
	void achieveTreasure();
	/**
	 * show that a round started
	 */
	void roundStart_Show();
	/**
	 * show that a round's treasure has changed.
	 */
	void treasureChange_Show();
        void playerMove(int i, BoardCell destinationCell);
}
