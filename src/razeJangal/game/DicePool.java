package razeJangal.game;

import java.util.Random;
import java.util.Scanner;

/**
 * represent a pair of dice
 * @author Mohammad ali baghershemirani
 *
 */
public class DicePool
{
	/** keep the first dice */
	private final Dice firstDice = new Dice();
	/** keep the second dice */
	private final Dice secondDice = new Dice();
	/** says that first dice is used after rolled or not*/
	private boolean isUsedFirstDice = false;
	/** says that second dice is used after rolled or not*/
	private boolean isUsedSecondDice = false;
	
        public static boolean cheatMode = false;
	/**
	 * roll both dices and set use of them into unused
	 */
	public void roll()
	{
		firstDice.roll();
		secondDice.roll();
		isUsedFirstDice = false;
		isUsedSecondDice = false;
	}
	
	/**
	 * set that first dice is used
	 * @see #isUsedFirstDice
	 * @see #useFirstDice()
	 * @see #useSecondDice()	 
	 */
	public void useFirstDice()
	{
		isUsedFirstDice = true;
	}
	
	/**
	 * set that second dice is used
	 * @see #isUsedSecondDice()
	 * @see #useFirstDice()
	 * @see #useSecondDice()
	 */
	public void useSecondDice()
	{
		isUsedSecondDice = true;
	}

	/**
	 * 
	 * @return a <code>boolean</code> that first dice is used or not
	 * @see #useFirstDice()
	 */
	public boolean isUsedFirstDice()
	{
		return isUsedFirstDice;
	}

	/**
	 * 
	 * @return a <code>boolean</code> that second dice is used or not
	 * @see #useSecondDice()
	 */
	public boolean isUsedSecondDice()
	{
		return isUsedSecondDice;
	}
	
	/**
	 * if when dice are double we use the abilities that are<br>
	 * gain by double, we used both dices.
	 * @return a <code> boolean</code>that we used from abilities that are came from double dice
	 */
	public boolean isUsedBothDice()
	{
		if (isUsedFirstDice && isUsedSecondDice)
			return true;
		return false;
	}
	
	/**
	 * 
	 * @return the face of first dice
	 */
	public int getFirstDiceFace()
	{
		return firstDice.getFace();
	}
	
	/**
	 * 
	 * @return the face of second dice
	 */
	public int getSecondDiceFace()
	{
		return secondDice.getFace();
	}
	
	/**
	 * 
	 * @return <code>true</code> if face of two dice are same, else returns <code>false</code>
	 */
	public boolean isDouble()
	{
		if (secondDice.getFace() == firstDice.getFace())
			return true;
		else
			return false;
	}

	/**
	 * use the abilities that are came from this fact that,<br>
	 * dice are <b>double</b>.
	 */
	public void useBothDice() 
	{
		isUsedFirstDice = true;
		isUsedSecondDice = true;		
	}
	
}

/**
 * represent a dice that have sixth face that are enumerated from 1 to 6
 * @author Mohammad ali baghershemirani
 *
 */
class Dice
{
	private final Random random = new Random();
	/** keep the face of dice that is on the top */
	private int face = 1;
	
	/**
	 * roll a dice, and have a new face from 1 to 6
	 */
	public void roll()
	{
            if (!DicePool.cheatMode)
		face = random.nextInt(6) + 1;
            else
                try
                {
                    System.out.println("Pay attention that Cell's will be dehighlight after you enter,+\n"
                            + "next player Dice,+\n"
                            + "IT'S NOT BUG!\n"
                            + "it's because you now that you are in cheat mode4");
                    System.out.println("Enter Your Dice : ");
                    face = new Scanner(System.in).nextInt();
                }catch(Exception e)
                {
                    face = 1;
                }
	}
	
	/**
	 * 
	 * @return the {@link #face} of dice
	 */
	public int getFace()
	{
		return face;
	}
}