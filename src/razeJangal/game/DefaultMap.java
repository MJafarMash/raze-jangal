package razeJangal.game;

import razeJangal.game.board.*;
import razeJangal.game.board.cell.*;

/**
 * defualt map have final {@link #CELLS} and {@link #CONNECTIONS} that are readed by<br>
 * {@link razeJangal.game.board.BoardLoader}
 * @author Mohammad ali baghershemirani
 *
 */
public class DefaultMap
{
	/** keep the <code>array of cells</code> */
	private static BoardCell[] CELLS;
	/** keep the <code>array of connections</code> */
	private static Connection[] CONNECTIONS ;
	
	/**
	 * 
	 * @return the {@link #CELLS}
	 */
	public static BoardCell[] getCells()
	{
		return CELLS;
	}
	
	/**
	 * 
	 * @return the {@link #CONNECTIONS}
	 */
	public static Connection[] getConnections()
	{
		return CONNECTIONS;
	}

	static 
	{
		BoardLoader defaultMap = new BoardLoader(".\\default.map");
		DefaultMap.CELLS = defaultMap.getCells();
		DefaultMap.CONNECTIONS = defaultMap.getConnections();
	}
}
