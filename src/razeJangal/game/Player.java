package razeJangal.game;

/**
 * represent a player in the game
 * @author Mohammad ali baghershemirani
 *
 */
public class Player
{
	/** keep the position of player*/
	private int position = 0;
	/** keep number of player*/
	private int number = 1;
	/** keep score of player*/
	private int score = 0;
	
	/**
	 * <h5>Player Constructor</h5>
	 * set number of player
	 * @param number
	 */
	public Player(int number)
	{
		this.number = number;
	}
	
	/**
	 * move player to position
	 * @param position
	 */
	public void setPosition(int position)
	{
		this.position = position;
	}
	
	/**
	 * 
	 * @return the position of player
	 */
	public int getPosition()
	{
		return position;
	}
	
	/**
	 * 
	 * @return the number of player
	 */
	public int getNumber()
	{
		return number;
	}
	
	@Override
	public String toString()
	{
		return "Player " + number;
	}

	/**
	 * 
	 * @return the score of player
	 */
	public int getScore() {
		return score;
	}

	/**
	 * add score of player
	 */
	public void addScore() {
		score++;
	}
}
